import { FC } from 'react';

import Sidebar from 'modules/sidebar';

const LayoutWithSidenav: FC = ({ children }) => {
  return (
    <div className="w-full flex">
      <div className="w-[300px] fixed top-0 left-0">
        <Sidebar />
      </div>
      <div className="w-screen ml-[300px] overflow-auto bg-gray-100">
        {children}
      </div>
    </div>
  );
};

export default LayoutWithSidenav;
