export type TimeBookingData = {
    dateDeparture: string;
    dateArrival: string;
    timeDeparture: string;
    timeArrival: string;
}

export type BookingData = {
    bus: string;
    route: string;
    timeBooking: TimeBookingData;
}

export default {}