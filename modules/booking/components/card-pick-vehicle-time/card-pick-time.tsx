import { Button, DatePicker, Space } from 'antd';
import moment from 'moment';
import { useState } from 'react';
import { useQuery } from 'react-query';
import { useSetRecoilState } from 'recoil';

import { indexSelector } from 'states';

enum TypeTime {
  departure = 'departure',
  arrival = 'arrival',
}

type PropsPickHour = {
  hours: string[];
  hour: string;
  type: keyof typeof TypeTime;
  onClickHandler: (time: string, type: keyof typeof TypeTime) => void;
};

const PickHour = ({ hour, hours, type, onClickHandler }: PropsPickHour) => {
  const isTimeOut = (time: string): boolean => {
    if (type === TypeTime.arrival) return false;
    return moment().isAfter(moment(time, 'hh:mm'));
  };

  return (
    <div className="border border-solid border-gray-300 p-2 rounded-lg w-full">
      <p className="text-gray-400 m-0 mt-1">
        {type === TypeTime.departure ? 'Jam Berangkat' : 'Jam Pulang'}
      </p>
      <Space direction="horizontal" size="small" wrap className="mt-3">
        {hours?.map((el: any, i: number) => (
          <Button
            onClick={() => onClickHandler(el, type)}
            size="small"
            key={i}
            className={`${hour === el ? 'bg-red-600 text-white' : ''}`}
            disabled={isTimeOut(el)}
          >
            {el}
          </Button>
        ))}
      </Space>
    </div>
  );
};

const CardPickTime = () => {
  const setIndexState = useSetRecoilState(indexSelector);

  const formatDate = 'D/M/yyyy';

  const defaultTimeBooking = {
    dateDeparture: moment(moment.now()).format(formatDate),
    dateArrival: '',
    timeDeparture: '',
    timeArrival: '',
  };

  const query = useQuery(['test-times'], async () => {
    const req = await new Promise((res, _) => {
      setTimeout(() => {
        res(['07:00', '13:00', '17:00', '23:00', '24:00']);
      }, 1000);
    });
    return req;
  });

  const [timeBooking, setTimeBooking] = useState(defaultTimeBooking);

  const onSelectDatePickerHandler = (
    value: moment.Moment | null,
    type: keyof typeof TypeTime
  ) => {
    if (type === TypeTime.departure) {
      setTimeBooking((prev) => ({
        ...prev,
        dateDeparture: value ? moment(value).format(formatDate) : '',
      }));
      return;
    }
    setTimeBooking((prev) => ({
      ...prev,
      dateArrival: value ? moment(value).format(formatDate) : '',
    }));
    if (!value) {
      setTimeBooking((prev) => ({
        ...prev,
        timeArrival: '',
      }));
    }
  };

  const onSelectTimeHandler = (time: string, type: keyof typeof TypeTime) => {
    if (type === TypeTime.departure) {
      setTimeBooking((prev) => ({
        ...prev,
        timeDeparture: time,
      }));
      return;
    }
    setTimeBooking((prev) => ({
      ...prev,
      timeArrival: time,
    }));
  };

  const isValidNext = () => {
    if (timeBooking.dateDeparture && !timeBooking.dateArrival) {
      return timeBooking.timeDeparture === '';
    }
    if (timeBooking.dateDeparture && timeBooking.dateArrival) {
      if (!timeBooking.timeArrival && !timeBooking.timeDeparture) return true;
      if (!timeBooking.timeArrival || !timeBooking.timeDeparture) return true;
      return false;
    }
    return false;
  };

  const onChooseHandler = () => {
    setIndexState((prev) => ({
      ...prev,
      stepBooking: 1,
    }));
  };

  return (
    <div className="flex w-full flex-col h-full items-start">
      <div className="w-full flex items-center rounded-lg overflow-hidden border border-solid border-gray-300">
        <div className="flex-1 p-3 border-none flex flex-col bg-white">
          Berangkat
          <DatePicker
            allowClear={false}
            value={moment(timeBooking.dateDeparture, formatDate)}
            suffixIcon={false}
            disabledDate={(current) => {
              return moment().add('-1', 'day') > current;
            }}
            onChange={(value) =>
              onSelectDatePickerHandler(value, TypeTime.departure)
            }
          />
        </div>
        <div className="h-full bg-slate-300" style={{ width: '1px' }}></div>
        <div className="flex-1 p-3 border-none flex flex-col bg-white">
          Pulang
          <DatePicker
            value={
              timeBooking.dateArrival !== ''
                ? moment(timeBooking.dateArrival, formatDate)
                : null
            }
            disabledDate={(current) => {
              return moment() > current;
            }}
            suffixIcon={false}
            onChange={(value) =>
              onSelectDatePickerHandler(value, TypeTime.arrival)
            }
            showToday={false}
            showNow={false}
          />
        </div>
      </div>
      <Space direction="vertical" className="w-full mt-3">
        {query.isLoading ? (
          <p className="">Loading</p>
        ) : (
          <>
            <PickHour
              hour={timeBooking.timeDeparture}
              onClickHandler={onSelectTimeHandler}
              hours={query.data as string[]}
              type={TypeTime.departure}
            />
            {timeBooking.dateArrival && (
              <PickHour
                hour={timeBooking.timeArrival}
                onClickHandler={onSelectTimeHandler}
                hours={query.data as string[]}
                type={TypeTime.arrival}
              />
            )}
          </>
        )}
      </Space>
      <div className="w-full flex justify-end mt-2">
        <Button
          onClick={onChooseHandler}
          type="primary"
          disabled={isValidNext()}
          className="w"
        >
          Pilih
        </Button>
      </div>
    </div>
  );
};

export default CardPickTime;
