const CardVehicle = () => {
  return (
    <div className="flex flex-col rounded-lg border border-gray-300 border-solid">
      <img
        src="/images/minibus.jpg"
        alt="bus"
        className="w-[150px] h-[150px] object-cover rounded-lg"
      />
      <div className="p-2 w-full">
        <p className="text-black text-sm m-0">Jurusan</p>
        <p className="text-gray-600 capitalize font-semibold text-lg m-0 mt-1">
          Medan - Banda Aceh
        </p>
      </div>
    </div>
  );
};

export default CardVehicle;
