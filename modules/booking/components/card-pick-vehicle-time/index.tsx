import CardPickTime from './card-pick-time';
import CardVehicle from './card-vehicle';
const CardPickVehicleTime = () => {
  return (
    <div className="flex w-full p-3 border border-solid bg-white border-gray-300 rounded-lg">
      <CardVehicle />
      <div className="w-4"></div>
      <CardPickTime />
    </div>
  );
};

export default CardPickVehicleTime;
