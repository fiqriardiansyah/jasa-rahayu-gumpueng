import { yupResolver } from '@hookform/resolvers/yup';
import { Form } from 'antd';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { dummyUnitWeight } from '../../data';
import { FDataExpedition } from '../../models';

import ControlledInputNumber from 'components/form/controlled-inputs/controlled-input-number';
import ControlledSelectInput from 'components/form/controlled-inputs/controlled-input-select';
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

const schema: yup.SchemaOf<FDataExpedition> = yup.object().shape({
  senderName: yup.string().required('Nama pengirim wajib diisi'),
  receiverName: yup.string().required('Nama penerima wajib diisi'),
  senderPhone: yup.string().required('Nomor pengirim wajib diisi'),
  receiverPhone: yup.string().required('Nomor penerima wajib diisi'),
  itemName: yup.string().required('Nama barang wajib diisi'),
  quantity: yup.number().required('Jumlah barang wajib diisi'),
  unit: yup.string().required('Satuan unit wajib diisi'),
  destination: yup.string().required('Tujuan pengirim wajib diisi'),
  price: yup.string().required('Ongkos kirim wajib diisi'),
});

const FormDataExpedition = () => {
  const [form] = Form.useForm();
  const {
    handleSubmit,
    control,
    formState: { isValid },
  } = useForm<FDataExpedition>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    console.log('submit booking', data);
  });

  return (
    <div className="bg-white w-full p-5 rounded-md">
      <p className="font-bold text-black capitalize m-0">
        Data Pemesanan Ekspedisi
      </p>
      <div className="grid gap-4 grid-cols-2"></div>
      <Form
        form={form}
        labelCol={{ span: 3 }}
        labelAlign="left"
        colon={false}
        style={{ width: '100%' }}
        onFinish={onSubmitHandler}
        layout="vertical"
      >
        <div className="w-full grid gap-4 grid-cols-2">
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="senderName"
            label="Nama pengirim"
            placeholder=""
          />
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="senderPhone"
            label="No Whatsapp/handphone pengirim"
            placeholder=""
          />
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="receiverName"
            label="Nama penerima"
            placeholder=""
          />
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="receiverPhone"
            label="No Whatsapp/handphone penerima"
            placeholder=""
          />
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="itemName"
            label="Nama barang"
            placeholder=""
          />
          <div className="flex items-center">
            <ControlledInputText
              control={control}
              labelCol={{ xs: 24 }}
              name="quantity"
              label="Jumlah"
              type="number"
              placeholder=""
            />
            <div className="w-10"></div>
            <ControlledSelectInput
              showSearch
              name="unit"
              label="Satuan unit"
              placeholder="Pilih"
              optionFilterProp="children"
              control={control}
              options={dummyUnitWeight}
            />
          </div>
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="destination"
            label="Tujuan pengiriman/diambil"
            placeholder=""
          />
          <ControlledInputNumber
            control={control}
            labelCol={{ xs: 24 }}
            name="price"
            label="Ongkos kirim"
            placeholder=""
          />
        </div>
      </Form>
    </div>
  );
};

export default FormDataExpedition;
