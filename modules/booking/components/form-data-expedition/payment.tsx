import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form } from 'antd';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { dummyMethodPayment } from '../../data';
import { FDataExpeditionPayment } from '../../models';

import ControlledSelectInput from 'components/form/controlled-inputs/controlled-input-select';

const schema: yup.SchemaOf<FDataExpeditionPayment> = yup.object().shape({
  paymentMethod: yup.string().required('Metode pembayaran wajib diisi'),
});

const FormDataPayment = () => {
  const [form] = Form.useForm();
  const {
    handleSubmit,
    control,
    formState: { isValid },
  } = useForm<FDataExpeditionPayment>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    console.log('submit booking', data);
  });

  return (
    <div className="bg-white w-full p-3 rounded-md">
      <p className="font-bold text-black capitalize m-0">
        Pembayaran Ekspedisi
      </p>
      <Form
        form={form}
        labelCol={{ span: 3 }}
        labelAlign="left"
        colon={false}
        style={{ width: '100%' }}
        onFinish={onSubmitHandler}
        layout="vertical"
      >
        <div className="w-full flex items-center">
          <ControlledSelectInput
            showSearch
            name="paymentMethod"
            label="Metode pembayaran"
            placeholder="Pilih"
            optionFilterProp="children"
            control={control}
            options={dummyMethodPayment}
          />
          <div className="w-10"></div>
          <div className="flex flex-col w-full self-start ">
            <div className="w-full flex items-center justify-between">
              <p className="font-bold capitalize m-0">total biaya</p>
              <p className="font-bold capitalize m-0">Rp. 120.000</p>
            </div>
            <Button type="primary" danger className="w-full mt-2">
              SUBMIT PEMESANAN
            </Button>
          </div>
        </div>
      </Form>
    </div>
  );
};

export default FormDataPayment;
