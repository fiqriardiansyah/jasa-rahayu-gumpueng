import { yupResolver } from '@hookform/resolvers/yup';
import { Form } from 'antd';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { dummyDest } from '../../data';
import { FDataBooking } from '../../models';

import ControlledInputNumber from 'components/form/controlled-inputs/controlled-input-number';
import ControlledSelectInput from 'components/form/controlled-inputs/controlled-input-select';
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

const schema: yup.SchemaOf<Partial<FDataBooking>> = yup.object().shape({
  name: yup.string().required('Nama wajib diisi'),
  phone: yup.string().required('No whatsapp/handphone wajib diisi'),
  departureFrom: yup.string().required('Berangkat dari wajib diisi'),
  arrivalDest: yup.string().required('Tujuan sampai wajib diisi'),
  price: yup.string().required('Harga wajib diisi'),
  additionRoute: yup.string(),
  additionDest: yup.string(),
  additionPrice: yup.string(),
});

const FormDataBooking = () => {
  const [form] = Form.useForm();
  const {
    handleSubmit,
    control,
    formState: { isValid },
  } = useForm<FDataBooking>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    console.log('submit booking', data);
  });

  return (
    <div className="w-full bg-white p-5 rounded-md">
      <p className="capitalize font-bold">data pemesanan penumpang (pergi)</p>
      <Form
        form={form}
        labelCol={{ span: 3 }}
        labelAlign="left"
        colon={false}
        style={{ width: '100%' }}
        onFinish={onSubmitHandler}
        layout="vertical"
      >
        <div className="w-full flex items-center">
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="name"
            label="Nama pemesan"
            placeholder=""
          />
          <div className="w-10"></div>
          <ControlledInputText
            control={control}
            labelCol={{ xs: 12 }}
            name="phone"
            label="No Whatsapp/Handphone"
            placeholder=""
          />
        </div>
        <div className="w-full flex items-center">
          <ControlledSelectInput
            showSearch
            name="departureFrom"
            label="Berangkat Dari"
            placeholder="Pilih"
            optionFilterProp="children"
            control={control}
            options={dummyDest}
          />
          <div className="w-10"></div>
          <ControlledSelectInput
            showSearch
            name="arrivalDest"
            label="Tujuan Sampai"
            placeholder="Pilih"
            optionFilterProp="children"
            control={control}
            options={dummyDest}
          />
          <div className="w-10"></div>
          <ControlledInputNumber
            control={control}
            labelCol={{ xs: 12 }}
            name="price"
            label="Harga"
            placeholder="Harga"
          />
        </div>
        <div className="w-full flex items-center">
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="additionRoute"
            label="Rute Tambahan/Lain"
            placeholder=""
          />
          <div className="w-10"></div>
          <ControlledInputText
            control={control}
            labelCol={{ xs: 12 }}
            name="additionDest"
            label="Tujuan Sampai"
            placeholder=""
          />
          <div className="w-10"></div>
          <ControlledInputNumber
            control={control}
            labelCol={{ xs: 12 }}
            name="additionPrice"
            label="Harga Tambahan"
            placeholder=""
          />
        </div>
      </Form>
    </div>
  );
};

export default FormDataBooking;
