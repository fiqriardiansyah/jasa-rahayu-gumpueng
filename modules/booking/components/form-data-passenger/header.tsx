import { useMemo } from 'react';
import { useRecoilValue } from 'recoil';

import { indexSelector } from 'states';

const FormDataHeader = () => {
  const indexState = useRecoilValue(indexSelector);

  const totalSeatLeft = useMemo(() => {
    let total = 0;
    if (!indexState.seat) return;
    [...indexState.seat.layout].forEach((rows) =>
      rows.forEach((seats) =>
        seats.forEach((seat) => {
          if (!seat.isPick && !seat.taken && seat.type === 'seat') {
            ++total;
          }
        })
      )
    );
    return total;
  }, [indexState.seat]);

  return (
    <div className="bg-white w-full p-3 rounded-md flex flex-col items-center">
      <p className="font-semibold text-black capitalize m-0">MINIBUS/HIACE</p>
      <p className="font-semibold text-black capitalize m-0">
        Banda Aceh - Medan
      </p>
      <p className="font-semibold text-black capitalize m-0">
        Jadwal Keberangkatan: 15/8/2022 - Jam 12:00 WIB
      </p>
      <p
        className={`${
          totalSeatLeft === 0 ? 'text-red-400' : 'text-black'
        } font-semibold capitalize m-0`}
      >
        {`Tempat duduk sisa: ${totalSeatLeft}`}
      </p>
    </div>
  );
};

export default FormDataHeader;
