import { useMemo } from 'react';
import { useRecoilValue } from 'recoil';

import { indexSelector } from 'states';

const FormDataPassenger = () => {
  const indexState = useRecoilValue(indexSelector);

  const totalPassenger = useMemo(() => {
    let total = 0;
    if (!indexState.seat) return;
    [...indexState.seat.layout].forEach((rows) =>
      rows.forEach((seats) =>
        seats.forEach((seat) => {
          if ((seat.isPick || seat.taken) && seat.type === 'seat') {
            ++total;
          }
        })
      )
    );
    return total;
  }, [indexState.seat]);

  return (
    <div className="w-full bg-white p-3 rounded-md justify-between flex items-center">
      <p className="capitalize font-bold m-0">jumlah penumpang</p>
      <p className="capitalize font-bold m-0">{`${totalPassenger} orang`}</p>
    </div>
  );
};

export default FormDataPassenger;
