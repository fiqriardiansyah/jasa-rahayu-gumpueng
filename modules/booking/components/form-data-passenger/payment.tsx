import { yupResolver } from '@hookform/resolvers/yup';
import { Form } from 'antd';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { dummyMethodPayment, dummySchemaPayment } from '../../data';
import { FDataPayment } from '../../models';

import ControlledInputNumber from 'components/form/controlled-inputs/controlled-input-number';
import ControlledSelectInput from 'components/form/controlled-inputs/controlled-input-select';

const schema: yup.SchemaOf<Partial<FDataPayment>> = yup.object().shape({
  paymentSchema: yup.string().required('Skema pembayaran wajib diisi'),
  paymentMethod: yup.string().required('Metode pembayaran wajib diisi'),
  total: yup.string().required('jumlah wajib diisi'),
});

const FormDataPayment = () => {
  const [form] = Form.useForm();
  const {
    handleSubmit,
    control,
    formState: { isValid },
  } = useForm<FDataPayment>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    console.log('submit booking', data);
  });

  return (
    <div className="w-full bg-white p-3 rounded-md">
      <p className="capitalize font-bold">pembayaran</p>
      <Form
        form={form}
        labelCol={{ span: 3 }}
        labelAlign="left"
        colon={false}
        style={{ width: '100%' }}
        onFinish={onSubmitHandler}
        layout="vertical"
      >
        <div className="w-full flex items-center">
          <ControlledSelectInput
            showSearch
            name="paymentSchema"
            label="Skema pembayaran"
            placeholder="Pilih"
            optionFilterProp="children"
            control={control}
            options={dummySchemaPayment}
          />
          <div className="w-10"></div>
          <ControlledInputNumber
            control={control}
            labelCol={{ xs: 12 }}
            name="total"
            label="Jumlah"
            placeholder=""
          />
        </div>
        <ControlledSelectInput
          showSearch
          name="paymentMethod"
          label="Metode pembayaran"
          placeholder="Pilih"
          optionFilterProp="children"
          control={control}
          options={dummyMethodPayment}
        />
      </Form>
    </div>
  );
};

export default FormDataPayment;
