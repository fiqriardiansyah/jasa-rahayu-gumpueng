import { Select } from 'antd';
import { useRecoilState } from 'recoil';
import SeatLayouting from '../seat-layouting';
import SeatDescription from '../seat-layouting/seat-description';

import { SeatProperty } from 'modules/booking/models';
import { indexSelector } from 'states';

import { useMemo } from 'react';
import { dummySeatLayoutMinimum } from '../../data';
import { seatLayouts, seatLayoutsOptions } from '../../data/seat';

const FormDataSeat = () => {
  const [indexState, setIndexState] = useRecoilState(indexSelector);

  const onClickSeat = (seatClick: SeatProperty) => {
    if (!indexState.seat) return;
    const copySeatLayout = {
      ...indexState.seat,
      layout: indexState.seat.layout.map((rows) =>
        rows.map((seats) =>
          seats.map((seat) => {
            if (seat.number === seatClick.number) {
              return {
                ...seat,
                isPick: !seatClick.isPick,
              };
            }
            return seat;
          })
        )
      ),
    };

    setIndexState((prev) => ({
      ...prev,
      seat: copySeatLayout,
    }));
  };

  const emptySeat = useMemo(() => {
    let empty = 0;
    if (!indexState.seat) return;
    [...indexState.seat.layout].forEach((rows) =>
      rows.forEach((seats) =>
        seats.forEach((seat) => {
          if (!seat.isPick && !seat.taken && seat.type === 'seat') {
            ++empty;
          }
        })
      )
    );
    return empty;
  }, [indexState.seat]);

  const pickSeat = useMemo(() => {
    let pick = 0;
    if (!indexState.seat) return;
    [...indexState.seat.layout].forEach((rows) =>
      rows.forEach((seats) =>
        seats.forEach((seat) => {
          if (seat.isPick && seat.type === 'seat') {
            ++pick;
          }
        })
      )
    );
    return pick;
  }, [indexState.seat]);

  const onSelect = (value: number) => {
    const layout = seatLayouts.find((el) => el.id === value);
    setIndexState((prev) => ({
      ...prev,
      seat: layout!.layout,
    }));
  };

  return (
    <div className="w-full bg-white p-3 rounded-md">
      <Select
        onSelect={onSelect}
        placeholder="test semua layout"
        options={seatLayoutsOptions}
      />
      <div className="w-full flex items-center justify-between">
        <p className="capitalize font-bold m-0">pilih tempat duduk</p>
        <div className="flex items-center">
          <p className="m-0 ml-2 capitalize">
            kosong:{' '}
            <span className="font-semibold text-lg m-0">{emptySeat}</span>
          </p>
          <p className="m-0 ml-2 capitalize">
            dipilih:{' '}
            <span className="font-semibold text-lg m-0">{pickSeat}</span>
          </p>
        </div>
      </div>
      <div className="h-5"></div>
      <SeatDescription />
      <div className="h-10"></div>
      <SeatLayouting
        seatLayout={indexState.seat || dummySeatLayoutMinimum}
        onClick={onClickSeat}
      />
    </div>
  );
};

export default FormDataSeat;
