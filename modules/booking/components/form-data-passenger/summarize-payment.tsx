import { Button } from 'antd';

const FormDataSummarizePayment = () => {
  return (
    <div className="w-full bg-white p-5 rounded-md">
      <p className="capitalize font-bold">resume pembayaran</p>
      <div className="w-full flex items-center justify-between">
        <p className="font-bold">Total biaya</p>
        <p className="font-bold">Rp. 450.000</p>
      </div>
      <div className="w-full flex items-center justify-between px-2">
        <p className="text-gray-400">Biaya 1 penumpang</p>
        <p className="text-gray-400">Rp. 450.000</p>
      </div>
      <div className="w-full flex items-center justify-between px-2">
        <p className="text-gray-400">Biaya lain</p>
        <p className="text-gray-400">Rp. -</p>
      </div>
      <hr className="bg-gray-500" />
      <div className="w-full flex items-center justify-between">
        <p className="font-bold">Pembayaran</p>
        <p className="font-bold">Rp. 450.000</p>
      </div>
      <div className="w-full flex items-center justify-between px-2">
        <p className="text-gray-400">Sisa pembayaran</p>
        <p className="text-gray-400">Rp. 450.000</p>
      </div>
      <Button type="primary" danger className="!w-full mt-5">
        SUBMIT PEMESANAN
      </Button>
    </div>
  );
};

export default FormDataSummarizePayment;
