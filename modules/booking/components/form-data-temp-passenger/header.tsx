const FormDataHeader = () => {
  return (
    <div className="bg-white w-full p-3 rounded-md flex flex-col items-center">
      <p className="font-semibold text-black capitalize m-0">MINIBUS/HIACE</p>
      <p className="font-semibold text-black capitalize m-0">
        Banda Aceh - Medan
      </p>
      <p className="font-semibold text-black capitalize m-0">
        Jadwal Keberangkatan: 15/8/2022 - Jam 12:00 WIB
      </p>
    </div>
  );
};

export default FormDataHeader;
