import { yupResolver } from '@hookform/resolvers/yup';
import { Form } from 'antd';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { FDataTempPassenger } from '../../models';

import ControlledInputNumber from 'components/form/controlled-inputs/controlled-input-number';
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

const schema: yup.SchemaOf<FDataTempPassenger> = yup.object().shape({
  from: yup.string().required('Berangkat dari wajib diisi'),
  to: yup.string().required('Tujuan wajib diisi'),
  quantity: yup.string().required('Jumlah penumpang wajib diisi'),
  price: yup.string().required('Harga wajib diisi'),
});

const FormDataTempPassenger = () => {
  const [form] = Form.useForm();
  const {
    handleSubmit,
    control,
    formState: { isValid },
  } = useForm<FDataTempPassenger>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    console.log('submit booking', data);
  });

  return (
    <div className="bg-white w-full p-5 rounded-md">
      <p className="font-bold text-black capitalize m-0">
        Data Pemesanan Penumpang Sementara
      </p>
      <div className="grid gap-4 grid-cols-2"></div>
      <Form
        form={form}
        labelCol={{ span: 3 }}
        labelAlign="left"
        colon={false}
        style={{ width: '100%' }}
        onFinish={onSubmitHandler}
        layout="vertical"
      >
        <div className="w-full grid gap-4 grid-cols-4">
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="from"
            label="Berangkat dari"
            placeholder=""
          />
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="to"
            label="Tujuan sampai"
            placeholder=""
          />
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="quantity"
            label="Jumlah penumpang"
            placeholder=""
            type="number"
          />
          <ControlledInputNumber
            control={control}
            labelCol={{ xs: 24 }}
            name="price"
            label="Harga"
            placeholder=""
          />
        </div>
      </Form>
    </div>
  );
};

export default FormDataTempPassenger;
