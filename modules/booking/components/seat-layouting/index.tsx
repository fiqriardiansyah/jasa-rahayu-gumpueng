import { useEffect, useRef } from 'react';

import Seat from './seat';

import { SeatLayout, SeatProperty } from 'modules/booking/models';
import Utils from 'utils';

type Props = {
  seatLayout: SeatLayout;
  onClick: (seat: SeatProperty) => void;
};

const SeatLayouting = ({ onClick, seatLayout }: Props) => {
  const containerRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    (() => {
      if (containerRef.current) {
        containerRef.current.style.gridTemplateAreas =
          Utils.generateGridArea(seatLayout);
      }
    })();
  }, [seatLayout]);

  return (
    <div className="w-full flex flex-col bg-slate-100 p-4 rounded-md">
      <div className="mb-4 text-white font-semibold text-lg w-full h-10 bg-slate-300 rounded-t-full flex items-center justify-center">
        Depan
      </div>
      <div ref={containerRef} className={`w-full grid gap-x-12 gap-y-4 `}>
        {seatLayout?.layout?.map((row, indexRow) => {
          return row.map((seats, indexSeats) => {
            return seats.map((seat) => {
              return (
                <div
                  className="flex items-center"
                  key={seat.number}
                  style={{
                    gridArea: `${String.fromCharCode(
                      Utils.charCodeA + indexRow
                    )}${indexRow}${indexSeats}`,
                  }}
                >
                  {seats.map((seat) => {
                    if (seat.type === 'seat') {
                      return (
                        <Seat onClick={onClick} key={seat.number} seat={seat} />
                      );
                    }
                  })}
                </div>
              );
            });
          });
        })}
      </div>
      <div className="mt-4 text-white font-semibold text-lg w-full h-10 bg-slate-300 rounded-b-full flex items-center justify-center">
        Belakang
      </div>
    </div>
  );
};

export default SeatLayouting;
