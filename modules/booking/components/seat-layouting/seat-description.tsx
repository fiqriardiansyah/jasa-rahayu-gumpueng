import { seatTypeDescription } from '../../data';

const SeatDescription = () => {
  return (
    <div className="w-full flex items-center">
      {Object.keys(seatTypeDescription).map((key) => {
        const seat =
          seatTypeDescription[key as keyof typeof seatTypeDescription];

        return (
          <div className="flex items-center mr-10" key={key}>
            <div
              className={`${seat.color} h-7 w-7 rounded text-white font-semibold text-3xl flex items-center justify-center mt-1`}
            />
            <p className="text-gray-400 capitalize text-sm m-0 ml-2">
              {seat.description}
            </p>
          </div>
        );
      })}
    </div>
  );
};

export default SeatDescription;
