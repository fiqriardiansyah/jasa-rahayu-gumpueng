import classNames from 'classnames';
import { SeatProperty } from '../../models';

import { seatTypeDescription } from '../../data';

export type Props = {
  seat: SeatProperty;
  onClick: (seat: SeatProperty) => void;
};

const Seat = ({ seat, onClick }: Props) => {
  const buttonClass = classNames(
    'border-none bg-transparent rounded-md py-2 px-0 pl-1 flex-1 flex flex-col items-center',
    {
      'cursor-pointer': !seat.taken,
      'cursor-not-allowed': seat.taken,
    }
  );

  const seatClass = classNames(
    ' w-full rounded h-[100px] text-white font-semibold text-3xl flex items-center justify-center mt-2',
    {
      [seatTypeDescription.notAvailable.color]: seat.taken,
      [seatTypeDescription.available.color]: !seat.taken,
      [seatTypeDescription.pick.color]: seat.isPick && !seat.taken,
    }
  );

  return (
    <button
      onClick={() => !seat.taken && onClick(seat)}
      key={seat.number}
      className={buttonClass}
    >
      <div className={seatClass}>{seat.number}</div>
    </button>
  );
};

export default Seat;
