import { Select, Space } from 'antd';
import { useQuery } from 'react-query';

export const dataCategoryId = [
  {
    label: 'Super patas 34',
    value: 1,
  },
  {
    label: 'Minibus/hiace',
    value: 2,
  },
];

type Props = {
  onSelect: (value: string) => void;
};

const SelectBusCategory = ({ onSelect }: Props) => {
  const query = useQuery(['test-getBusCategory'], async () => {
    const req = await new Promise((res, _) => {
      setTimeout(() => {
        res(dataCategoryId);
      }, 1000);
    });
    return req;
  });

  return (
    <Space direction="vertical" size="small" className="w-full flex-1">
      <p className="text-gray-800 font-semibold text-lg">Kategori bus</p>
      <Select
        loading={query.isLoading}
        placeholder="Semua"
        onSelect={onSelect}
        className="w-full"
      >
        {dataCategoryId?.map((option) => {
          return (
            <Select.Option value={option.value} key={option.value}>
              {option.label}
            </Select.Option>
          );
        })}
      </Select>
    </Space>
  );
};

export default SelectBusCategory;
