import { Select, Space } from 'antd';
import { useQuery } from 'react-query';

export const dataCategoryId = [
  {
    label: 'Medan - Banda Aceh',
    value: 1,
  },
  {
    label: 'Banda Aceh - Medan',
    value: 2,
  },
];

type Props = {
  onSelect: (value: string) => void;
};

const SelectRoute = ({ onSelect }: Props) => {
  const query = useQuery(['test-getRoute'], async () => {
    const req = await new Promise((res, _) => {
      setTimeout(() => {
        res(dataCategoryId);
      }, 1000);
    });
    return req;
  });

  return (
    <Space direction="vertical" size="small" className="w-full flex-1">
      <p className="text-gray-800 font-semibold text-lg">Trayek</p>
      <Select
        loading={query.isLoading}
        placeholder="Semua"
        onSelect={onSelect}
        className="w-full"
      >
        {dataCategoryId?.map((option) => {
          return (
            <Select.Option value={option.value} key={option.value}>
              {option.label}
            </Select.Option>
          );
        })}
      </Select>
    </Space>
  );
};

export default SelectRoute;
