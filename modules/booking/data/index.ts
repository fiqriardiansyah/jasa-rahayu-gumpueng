import { SeatLayout } from '../models';

export const dummySeatLayout: SeatLayout = {
    name: 'minibus/hiace',
    column: 3,
    layout: [
        [[{ number: 1, isPick: false, taken: true, type: 'seat' }], [{ type: 'non-seat' }], [{ type: 'non-seat' }]],
        [[{ number: 2, isPick: false, taken: false, type: 'seat' }, { number: 3, isPick: false, taken: false, type: 'seat' }, { number: 4, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 5, isPick: false, taken: false, type: 'seat' }], [{ number: 6, isPick: false, taken: false, type: 'seat' }, { number: 7, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 8, isPick: false, taken: false, type: 'seat' }], [{ number: 9, isPick: false, taken: true, type: 'seat' }, { number: 10, isPick: false, taken: true, type: 'seat' }]],
    ]
}

export const dummySeatLayoutMinimum: SeatLayout = {
    name: '',
    column: 0,
    layout: []
}

export const seatTypeDescription = {
    available: {
        color: 'bg-green-300',
        description: 'kosong'
    },
    notAvailable: {
        color: 'bg-gray-300',
        description: 'penuh'
    },
    pick: {
        color: 'bg-orange-200',
        description: 'dipilih'
    }
}

export const forms = [
    {
        id: 1,
        name: 'penumpang',
    },
    {
        id: 2,
        name: 'ekspedisi',
    },
    {
        id: 3,
        name: 'penumpang sementara',
    }
];

export const dummyDest = [
    {
        label: "Banda Aceh",
        value: 1,
    },
    {
        label: "Medan",
        value: 2,
    }
]

export const dummySchemaPayment = [
    {
        label: "Lunas",
        value: 1,
    },
    {
        label: "Dp",
        value: 2,
    },
]

export const dummyMethodPayment = [
    {
        label: "Tunai",
        value: 1,
    },
    {
        label: "Transfer - Bank BSI",
        value: 2,
    },
    {
        label: "Transfer - Bank BRI",
        value: 2,
    },
]

export const dummyUnitWeight = [
    {
        label: "KG",
        value: 1,
    },
    {
        label: "QUINTAL",
        value: 2,
    },
]

export const dummyTempPessengerPayment = [
    {
        label: "Supir",
        value: 1,
    },
    {
        label: "Transfer - BSI",
        value: 2,
    },
    {
        label: "Transfer - BCA",
        value: 3,
    },
]