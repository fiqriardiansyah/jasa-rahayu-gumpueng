import { SeatLayout } from "../models";

export const seatLayout1: SeatLayout = {
    name: 'minibus/hiace',
    column: 3,
    layout: [
        [[{ number: 1, isPick: false, taken: true, type: 'seat' }], [{ type: 'non-seat' }], [{ type: 'non-seat' }]],
        [[{ number: 2, isPick: false, taken: false, type: 'seat' }, { number: 3, isPick: false, taken: false, type: 'seat' }, { number: 4, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 5, isPick: false, taken: false, type: 'seat' }], [{ number: 6, isPick: false, taken: false, type: 'seat' }, { number: 7, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 8, isPick: false, taken: false, type: 'seat' }], [{ number: 9, isPick: false, taken: true, type: 'seat' }, { number: 10, isPick: false, taken: true, type: 'seat' }]],
    ]
}

export const seatLayout2: SeatLayout = {
    name: 'super nonstop 1-2',
    column: 3,
    layout: [
        [[{ number: '1A', isPick: false, taken: false, type: 'seat' }], [{ number: '1B', isPick: false, taken: false, type: 'seat' }, { number: '1C', isPick: false, taken: false, type: 'seat' }]],
        [[{ number: '2A', isPick: false, taken: true, type: 'seat' }], [{ number: '2B', isPick: false, taken: true, type: 'seat' }, { number: '2C', isPick: false, taken: true, type: 'seat' }]],
        [[{ number: '3A', isPick: false, taken: true, type: 'seat' }], [{ number: '3B', isPick: false, taken: true, type: 'seat' }, { number: '3C', isPick: false, taken: true, type: 'seat' }]],
        [[{ number: '4A', isPick: false, taken: true, type: 'seat' }], [{ number: '4B', isPick: false, taken: false, type: 'seat' }, { number: '4C', isPick: false, taken: false, type: 'seat' }]],
        [[{ type: 'non-seat' }], [{ number: '5B', isPick: false, taken: false, type: 'seat' }, { number: '5C', isPick: false, taken: false, type: 'seat' }]],
        [[{ number: '6A', isPick: false, taken: false, type: 'seat' }], [{ number: '6B', isPick: false, taken: false, type: 'seat' }, { number: '6C', isPick: false, taken: false, type: 'seat' }]],
        [[{ number: '7A', isPick: false, taken: false, type: 'seat' }], [{ number: '7B', isPick: false, taken: false, type: 'seat' }, { number: '7C', isPick: false, taken: false, type: 'seat' }]],
        [[{ number: '8A', isPick: false, taken: false, type: 'seat' }], [{ number: '8B', isPick: false, taken: false, type: 'seat' }, { number: '8C', isPick: false, taken: false, type: 'seat' }]],
    ]
}

export const seatLayout3: SeatLayout = {
    name: 'super nonstop 1-1',
    column: 3,
    layout: [
        [[{ number: '1A', isPick: false, taken: true, type: 'seat' }], [{ number: '1B', isPick: false, taken: true, type: 'seat' }], [{ number: '1C', isPick: false, taken: true, type: 'seat' }]],
        [[{ number: '2A', isPick: false, taken: true, type: 'seat' }], [{ number: '2B', isPick: false, taken: false, type: 'seat' }], [{ number: '2C', isPick: false, taken: false, type: 'seat' }]],
        [[{ number: '3A', isPick: false, taken: false, type: 'seat' }], [{ number: '3B', isPick: false, taken: false, type: 'seat' }], [{ number: '3C', isPick: false, taken: false, type: 'seat' }]],
        [[{ type: "non-seat" }], [{ type: 'non-seat' }], [{ number: '4C', isPick: false, taken: false, type: 'seat' }]],
        [[{ type: "non-seat" }], [{ type: 'non-seat' }], [{ number: '5C', isPick: false, taken: false, type: 'seat' }]],
        [[{ number: '6A', isPick: false, taken: false, type: 'seat' }], [{ number: '6B', isPick: false, taken: false, type: 'seat' }], [{ number: '6C', isPick: false, taken: false, type: 'seat' }]],
        [[{ number: '7A', isPick: false, taken: false, type: 'seat' }], [{ number: '7B', isPick: false, taken: false, type: 'seat' }], [{ number: '7C', isPick: false, taken: false, type: 'seat' }]],
        [[{ number: '8A', isPick: false, taken: false, type: 'seat' }], [{ number: '8B', isPick: false, taken: false, type: 'seat' }], [{ number: '8C', isPick: false, taken: false, type: 'seat' }]],
        [[{ number: '9A', isPick: false, taken: false, type: 'seat' }], [{ number: '9B', isPick: false, taken: false, type: 'seat' }], [{ number: '9C', isPick: false, taken: false, type: 'seat' }]],
    ]
}

export const seatLayout4: SeatLayout = {
    name: 'super patas',
    column: 4,
    layout: [
        [[{ number: 1, isPick: false, taken: false, type: 'seat' }, { number: 2, isPick: false, taken: false, type: 'seat' }], [{ number: 3, isPick: false, taken: false, type: 'seat' }, { number: 4, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 5, isPick: false, taken: false, type: 'seat' }, { number: 6, isPick: false, taken: false, type: 'seat' }], [{ number: 7, isPick: false, taken: false, type: 'seat' }, { number: 8, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 9, isPick: false, taken: true, type: 'seat' }, { number: 10, isPick: false, taken: true, type: 'seat' }], [{ number: 11, isPick: false, taken: true, type: 'seat' }, { number: 12, isPick: false, taken: true, type: 'seat' }]],
        [[{ number: 13, isPick: false, taken: true, type: 'seat' }, { number: 14, isPick: false, taken: true, type: 'seat' }], [{ number: 15, isPick: false, taken: true, type: 'seat' }, { number: 16, isPick: false, taken: true, type: 'seat' }]],
        [[{ type: 'non-seat' }, { type: 'non-seat' }], [{ number: 17, isPick: false, taken: true, type: 'seat' }, { number: 18, isPick: false, taken: true, type: 'seat' }]],
        [[{ type: 'non-seat' }, { type: 'non-seat' }], [{ number: 19, isPick: false, taken: true, type: 'seat' }, { number: 20, isPick: false, taken: true, type: 'seat' }]],
        [[{ number: 21, isPick: false, taken: true, type: 'seat' }, { number: 22, isPick: false, taken: false, type: 'seat' }], [{ number: 23, isPick: false, taken: false, type: 'seat' }, { number: 24, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 25, isPick: false, taken: false, type: 'seat' }, { number: 26, isPick: false, taken: false, type: 'seat' }], [{ number: 27, isPick: false, taken: false, type: 'seat' }, { number: 28, isPick: false, taken: false, type: 'seat' }]],
    ]
}

export const seatLayout5: SeatLayout = {
    name: 'patas',
    column: 4,
    layout: [
        [[{ number: 1, isPick: false, taken: false, type: 'seat' }, { number: 2, isPick: false, taken: false, type: 'seat' }], [{ number: 3, isPick: false, taken: false, type: 'seat' }, { number: 4, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 5, isPick: false, taken: false, type: 'seat' }, { number: 6, isPick: false, taken: false, type: 'seat' }], [{ number: 7, isPick: false, taken: false, type: 'seat' }, { number: 8, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 9, isPick: false, taken: false, type: 'seat' }, { number: 10, isPick: false, taken: false, type: 'seat' }], [{ number: 11, isPick: false, taken: false, type: 'seat' }, { number: 12, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 13, isPick: false, taken: false, type: 'seat' }, { number: 14, isPick: false, taken: false, type: 'seat' }], [{ number: 15, isPick: false, taken: false, type: 'seat' }, { number: 16, isPick: false, taken: false, type: 'seat' }]],
        [[{ type: 'non-seat' }, { type: 'non-seat' }], [{ number: 17, isPick: false, taken: false, type: 'seat' }, { number: 18, isPick: false, taken: false, type: 'seat' }]],
        [[{ number: 19, isPick: false, taken: true, type: 'seat' }, { number: 20, isPick: false, taken: true, type: 'seat' }], [{ number: 21, isPick: false, taken: true, type: 'seat' }, { number: 22, isPick: false, taken: true, type: 'seat' }]],
        [[{ number: 23, isPick: false, taken: true, type: 'seat' }, { number: 24, isPick: false, taken: true, type: 'seat' }], [{ number: 25, isPick: false, taken: true, type: 'seat' }, { number: 26, isPick: false, taken: true, type: 'seat' }]],
        [[{ number: 27, isPick: false, taken: true, type: 'seat' }, { number: 28, isPick: false, taken: true, type: 'seat' }], [{ number: 29, isPick: false, taken: true, type: 'seat' }, { number: 30, isPick: false, taken: true, type: 'seat' }]],
        [[{ number: 31, isPick: false, taken: true, type: 'seat' }, { number: 32, isPick: false, taken: false, type: 'seat' }], [{ number: 33, isPick: false, taken: false, type: 'seat' }, { number: 34, isPick: false, taken: false, type: 'seat' }]],
    ]
}

export const seatLayoutsOptions = [seatLayout1.name, seatLayout2.name, seatLayout3.name, seatLayout4.name, seatLayout5.name].map((el, i) => ({ value: i + 1, label: el }));

export const seatLayouts = [seatLayout1, seatLayout2, seatLayout3, seatLayout4, seatLayout5].map((el, i) => ({
    id: i + 1,
    layout: el,
}));