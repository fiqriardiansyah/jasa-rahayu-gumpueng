import { Space } from 'antd';
import { FC } from 'react';

import FormDataExpedition from 'modules/booking/components/form-data-expedition/expedition';
import FormDataHeader from 'modules/booking/components/form-data-expedition/header';
import FormDataPayment from 'modules/booking/components/form-data-expedition/payment';

const FormExpedition: FC = () => {
  return (
    <div className="w-full">
      <Space direction="vertical" className="w-full">
        <FormDataHeader />
        <FormDataExpedition />
        <FormDataPayment />
      </Space>
    </div>
  );
};

export default FormExpedition;
