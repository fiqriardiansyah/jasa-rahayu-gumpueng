import { Space } from 'antd';
import { FC, useEffect } from 'react';

import { useSetRecoilState } from 'recoil';

import { seatLayout2 } from '../../data/seat';

import { indexSelector } from 'states';

import FormDataBooking from 'modules/booking/components/form-data-passenger/booking';
import FormDataHeader from 'modules/booking/components/form-data-passenger/header';
import FormDataPassenger from 'modules/booking/components/form-data-passenger/passenger';
import FormDataPayment from 'modules/booking/components/form-data-passenger/payment';
import FormDataSeat from 'modules/booking/components/form-data-passenger/seat';
import FormDataSummarizePayment from 'modules/booking/components/form-data-passenger/summarize-payment';

const FormPassenger: FC = () => {
  const setIndexState = useSetRecoilState(indexSelector);

  useEffect(() => {
    // simulate get data
    setTimeout(() => {
      setIndexState((prev) => ({
        ...prev,
        seat: seatLayout2,
      }));
    }, 2000);
  }, []);

  return (
    <div className="w-full">
      <Space direction="vertical" className="w-full">
        <FormDataHeader />
        <FormDataBooking />
        <div className="w-full flex">
          <div className="w-full flex flex-col">
            <FormDataPassenger />
            <div className="h-2"></div>
            <FormDataSeat />
          </div>
          <div className="w-4"></div>
          <div className="w-full flex flex-col">
            <FormDataPayment />
            <div className="h-2"></div>
            <FormDataSummarizePayment />
          </div>
        </div>
      </Space>
    </div>
  );
};

export default FormPassenger;
