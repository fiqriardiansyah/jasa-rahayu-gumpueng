import { Space } from 'antd';
import { FC } from 'react';

import FormDataHeader from 'modules/booking/components/form-data-temp-passenger/header';
import FormDataPayment from 'modules/booking/components/form-data-temp-passenger/payment';
import FormDataTempPassenger from 'modules/booking/components/form-data-temp-passenger/temp-passenger';

const FormTempPassenger: FC = () => {
  return (
    <div className="w-full">
      <Space direction="vertical" className="w-full">
        <FormDataHeader />
        <FormDataTempPassenger />
        <FormDataPayment />
      </Space>
    </div>
  );
};

export default FormTempPassenger;
