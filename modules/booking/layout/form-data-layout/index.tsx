import { LeftOutlined } from '@ant-design/icons';
import { Button, Space } from 'antd';
import { useState } from 'react';
import { useSetRecoilState } from 'recoil';

import { indexSelector } from 'states';

import { forms } from '../../data';

// forms
import FormExpedition from './form-expedition';
import FormPassenger from './form-passenger';
import FormTemporaryPassenger from './form-temporary-passenger';

const FormDataLayout = () => {
  const indexState = useSetRecoilState(indexSelector);

  const [activeForm, setActiveForm] = useState(forms[0]);

  const onClickTypeForm = (form: typeof forms[0]) => {
    setActiveForm(form);
  };

  return (
    <div className="w-full bg-gray-100">
      <Space
        direction="horizontal"
        size="middle"
        className="w-full shadow p-2 bg-white sticky top-0 z-10"
      >
        <Button
          type="primary"
          icon={<LeftOutlined />}
          onClick={() => {
            indexState((prev) => ({
              ...prev,
              stepBooking: 0,
            }));
          }}
        >
          Kembali
        </Button>
        {forms.map((el) => (
          <Button
            key={el.id}
            type="ghost"
            size="large"
            onClick={() => onClickTypeForm(el)}
            className={`${
              activeForm.id === el.id ? 'text-black' : 'text-gray-300'
            } capitalize border-none shadow-none font-semibold`}
          >
            {el.name}
          </Button>
        ))}
      </Space>
      <div className="w-full p-3 pb-10">
        {activeForm.id === 1 && <FormPassenger />}
        {activeForm.id === 2 && <FormExpedition />}
        {activeForm.id === 3 && <FormTemporaryPassenger />}
      </div>
    </div>
  );
};

export default FormDataLayout;
