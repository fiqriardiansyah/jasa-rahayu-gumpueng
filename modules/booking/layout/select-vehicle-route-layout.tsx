import SelectBusCategory from '../components/select-bus-category';
import SelectRoute from '../components/select-route';

import CardPickVehicleTime from '../components/card-pick-vehicle-time';

const SelectVehicleRouteLayout = () => {
  const onSelectBusCategory = (value: string) => {
    console.log(value);
  };

  const onSelectRoute = (value: string) => {};

  return (
    <div className="w-full p-5 bg-gray-100">
      <div className="w-full bg-white shadow-md flex items-center p-5 rounded-lg border border-gray-300 border-solid sticky top-0 z-10">
        <SelectBusCategory onSelect={onSelectBusCategory} />
        <div className="w-5"></div>
        <SelectRoute onSelect={onSelectRoute} />
      </div>
      <p className="text-gray-600 text-xl font-semibold my-4">Minibus/Hiace</p>
      <div className="grid gap-4 grid-cols-2 2xl:grid-cols-3 px-4">
        {[...new Array(2)].map((el, i) => (
          <CardPickVehicleTime key={el} />
        ))}
      </div>
      <p className="text-gray-600 text-xl font-semibold my-4">
        Super Patas 32 seat
      </p>
      <div className="grid gap-4 grid-cols-2 2xl:grid-cols-3 px-4">
        {[...new Array(3)].map((el, i) => (
          <CardPickVehicleTime key={el} />
        ))}
      </div>
    </div>
  );
};

export default SelectVehicleRouteLayout;
