export type FDataBooking = {
    name: string;
    phone: string;
    departureFrom: string;
    arrivalDest: string;
    price: string;
    additionRoute: string;
    additionDest: string;
    additionPrice: string;
}

export type FDataPayment = {
    paymentSchema: string;
    total: string;
    paymentMethod: string;
}

export type FDataExpedition = {
    senderName: string;
    receiverName: string;
    senderPhone: string;
    receiverPhone: string;
    itemName: string;
    quantity: number;
    unit: string;
    destination: string;
    price: string;
}

export type FDataExpeditionPayment = Pick<FDataPayment, 'paymentMethod'>;

export type FDataTempPassenger = {
    from: string;
    to: string;
    quantity: string;
    price: string;
}

export type SeatType = 'seat' | 'non-seat'

export type SeatProperty = {
    number?: string | number;
    taken?: boolean | null;
    isPick?: boolean | null; // ispick is temporary choosen/ not fix taken
    type: SeatType;
}

export type SeatLayout = {
    name: string;
    column: number;
    layout: SeatProperty[][][];
}