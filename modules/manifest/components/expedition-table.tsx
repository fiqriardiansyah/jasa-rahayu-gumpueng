import { Button, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { TDataExpedition } from '../models';

import { dataExpedition } from '../data';

type Props = {
  onClickPrintReceipt: (data: TDataExpedition) => void;
};

const ExpeditionTable = ({ onClickPrintReceipt }: Props) => {
  const columns: ColumnsType<TDataExpedition> = [
    {
      title: 'Nomor resi',
      dataIndex: 'receipt_number',
      fixed: 'left',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Nama pengirim',
      dataIndex: 'sender_name',
      fixed: 'left',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Nomor hp pengirim',
      dataIndex: 'sender_phone',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Nama penerima',
      dataIndex: 'receiver_name',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Nomor hp penerima',
      dataIndex: 'receiver_phone',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Nama barang',
      dataIndex: 'item_name',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Tujuan pengiriman',
      dataIndex: 'destination',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Total biaya',
      dataIndex: 'price',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Loket booking',
      dataIndex: 'station_booking',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Cetak resi',
      fixed: 'right',
      render: (text, record) => (
        <Button
          onClick={() => onClickPrintReceipt(record)}
          className="capitalize"
        >
          Cetak
        </Button>
      ),
    },
  ];

  return (
    <Table
      scroll={{ x: 1500 }}
      columns={columns}
      dataSource={dataExpedition}
      className="w-full"
      pagination={false}
    />
  );
};

export default ExpeditionTable;
