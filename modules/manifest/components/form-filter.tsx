import { yupResolver } from '@hookform/resolvers/yup';
import { Form } from 'antd';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { dummyRoute, dummyStatus } from '../data';
import { FDataFilter } from '../models';

import ControlledInputDate from 'components/form/controlled-inputs/controlled-input-date';
import ControlledSelectInput from 'components/form/controlled-inputs/controlled-input-select';
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

const schema: yup.SchemaOf<Partial<FDataFilter>> = yup.object().shape({
  id_manifest: yup.string(),
  driver_name: yup.string(),
  vehicle: yup.string(),
  route: yup.string(),
  date: yup.string(),
  status: yup.string(),
});

const FormFilter = () => {
  const [form] = Form.useForm();
  const {
    handleSubmit,
    control,
    formState: { isValid },
  } = useForm<FDataFilter>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    console.log('submit booking', data);
  });

  return (
    <div className="w-full bg-white px-5 pt-5 rounded-md sticky top-0 z-30 shadow">
      <Form
        form={form}
        labelAlign="left"
        colon={false}
        onFinish={onSubmitHandler}
        layout="vertical"
      >
        <div className="grid grid-cols-2 gap-x-10">
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="id_manifest"
            label=""
            placeholder="ID Manifes"
          />
          <ControlledSelectInput
            showSearch
            name="route"
            label=""
            placeholder="Trayek"
            optionFilterProp="children"
            control={control}
            options={dummyRoute}
          />
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="driver_name"
            label=""
            placeholder="Nama Supir"
          />
          <ControlledInputDate
            control={control}
            name="date"
            label=""
            placeholder="Tanggal"
            picker="year"
            labelCol={{ xs: 12 }}
          />
          <ControlledInputText
            control={control}
            labelCol={{ xs: 24 }}
            name="vehicle"
            label=""
            placeholder="Kendaraan"
          />
          <ControlledSelectInput
            showSearch
            name="status"
            label=""
            placeholder="Status"
            optionFilterProp="children"
            control={control}
            options={dummyStatus}
          />
        </div>
      </Form>
    </div>
  );
};

export default FormFilter;
