import { Button, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { TDataManifest } from '../models';

import { data } from '../data';

type Props = {
  onClickDetail: (data: TDataManifest) => void;
};

const ManifestTable = ({ onClickDetail }: Props) => {
  const columns: ColumnsType<TDataManifest> = [
    {
      title: 'ID',
      dataIndex: 'id',
      fixed: 'left',
      render: (text, record) => (
        <Button
          onClick={() => onClickDetail(record)}
          className="capitalize"
          type="link"
        >
          {text}
        </Button>
      ),
    },
    {
      title: 'Jenis Bus',
      dataIndex: 'type_bus',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Trayek',
      dataIndex: 'route',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Tanggal',
      dataIndex: 'date',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Jam',
      dataIndex: 'hour',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Status',
      dataIndex: 'status',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Pembayaran',
      dataIndex: 'payment',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Kendaraan',
      dataIndex: 'vehicle',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Supir',
      dataIndex: 'driver',
      render: (text) => <p className="capitalize">{text}</p>,
    },
  ];

  return (
    <Table
      scroll={{ x: 1500 }}
      columns={columns}
      dataSource={data}
      className="w-full mt-10"
    />
  );
};

export default ManifestTable;
