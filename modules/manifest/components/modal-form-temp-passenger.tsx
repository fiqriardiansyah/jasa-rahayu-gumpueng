import { Modal } from 'antd';
import { useState } from 'react';

import FormTempPassenger from 'modules/booking/layout/form-data-layout/form-temporary-passenger';

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  closeModal: () => void;
};

type Props = {
  loading: boolean;
  children: (data: ChildrenProps) => void;
};

const ModalFormTempPassenger = ({ loading, children }: Props) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const closeModal = () => {
    if (loading) return;
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    closeModal,
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title="Penumpang sementara"
        open={isModalOpen}
        onCancel={closeModal}
        width={1000}
        footer={null}
      >
        <FormTempPassenger />
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalFormTempPassenger;
