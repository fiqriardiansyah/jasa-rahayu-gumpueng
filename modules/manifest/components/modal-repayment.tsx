import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form, Modal, Row, Space } from 'antd';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { FDataRepayment, TDataPassenger } from '../models';

// components
import ControlledInputDate from 'components/form/controlled-inputs/controlled-input-date';
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  closeModal: () => void;
  openModalWithData: (data: string | undefined) => void;
};

type Props = {
  onSubmit: (data: FDataRepayment, callback: () => void) => void;
  loading: boolean;
  children: (data: ChildrenProps) => void;
};

const schema: yup.SchemaOf<FDataRepayment> = yup.object().shape({
  name: yup.string().required('Nama yang membayar lunas wajib diisi'),
  date: yup.string().required('Tanggal lunas wajib diisi'),
});

const ModalRepayment = ({ onSubmit, loading, children }: Props) => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const {
    handleSubmit,
    control,
    formState: { isValid },
  } = useForm<FDataRepayment>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    onSubmit(data, () => {
      closeModal();
    });
  });

  const closeModal = () => {
    if (loading) return;
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const openModalWithData = (data: string | undefined) => {
    if (data) {
      const busService = JSON.parse(data) as TDataPassenger;
      openModal();
    }
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    closeModal,
    openModalWithData,
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title="Pelunasan pembayaran"
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Form
          form={form}
          labelCol={{ span: 3 }}
          labelAlign="left"
          disabled={loading}
          colon={false}
          style={{ width: '100%' }}
          onFinish={onSubmitHandler}
          layout="vertical"
        >
          <Space direction="vertical" className="w-full">
            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="name"
              label="Nama pelunas"
              placeholder="Nama"
            />

            <ControlledInputDate
              control={control}
              name="date"
              label="Tanggal lunas"
              labelCol={{ xs: 12 }}
            />

            <Row justify="start">
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading}
                  disabled={!isValid}
                >
                  Simpan
                </Button>
                <Button type="primary" danger onClick={closeModal}>
                  Batalkan
                </Button>
              </Space>
            </Row>
          </Space>
        </Form>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalRepayment;
