import { Button, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { TDataPassenger } from '../models';

import { dataPassenger } from '../data';

type Props = {
  onClickPrintTicket: (data: TDataPassenger) => void;
  onClickRemainingPayment: (data: TDataPassenger) => void;
};

const PassengerTable = ({
  onClickPrintTicket,
  onClickRemainingPayment,
}: Props) => {
  const columns: ColumnsType<TDataPassenger> = [
    {
      title: 'Kode Booking',
      dataIndex: 'booking_code',
      fixed: 'left',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Nama',
      dataIndex: 'name',
      fixed: 'left',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Nomor handphone',
      dataIndex: 'phone',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Kursi',
      dataIndex: 'seat',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Berangkat dari',
      dataIndex: 'from',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Tujuan',
      dataIndex: 'to',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Total',
      dataIndex: 'price',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Schema pembayaran',
      dataIndex: 'schema_payment',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Loket Booking',
      dataIndex: 'station_booking',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Pelunasan',
      dataIndex: 'repayment',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Sisa pembayaran',
      dataIndex: 'remaining_payment',
      fixed: 'right',
      render: (text, record) => (
        <Button
          onClick={() => onClickRemainingPayment(record)}
          className="capitalize"
          type="link"
          danger
        >
          {text}
        </Button>
      ),
    },
    {
      title: 'Cetak tiket',
      fixed: 'right',
      render: (text, record) => (
        <Button
          onClick={() => onClickPrintTicket(record)}
          className="capitalize"
        >
          Cetak
        </Button>
      ),
    },
  ];

  return (
    <Table
      scroll={{ x: 1500 }}
      columns={columns}
      dataSource={dataPassenger}
      className="w-full"
      pagination={false}
    />
  );
};

export default PassengerTable;
