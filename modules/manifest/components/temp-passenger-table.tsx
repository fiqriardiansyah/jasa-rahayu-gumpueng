import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { TDataTemporaryPassenger } from '../models';

import { dataTempPassenger } from '../data';

const TempPassengerTable = () => {
  const columns: ColumnsType<TDataTemporaryPassenger> = [
    {
      title: 'Berangkat dari',
      dataIndex: 'from',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Tujuan sampai',
      dataIndex: 'to',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Jumlah penumpang',
      dataIndex: 'total_passenger',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Total biaya',
      dataIndex: 'price',
      render: (text) => <p className="capitalize">{text}</p>,
    },
  ];

  return (
    <Table
      columns={columns}
      dataSource={dataTempPassenger}
      className="w-full"
      pagination={false}
    />
  );
};

export default TempPassengerTable;
