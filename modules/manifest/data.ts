import { TDataExpedition, TDataManifest, TDataPassenger, TDataTemporaryPassenger } from './models';

export const data: TDataManifest[] = [...new Array(20)].map((el, i) => ({
    key: i,
    id: '245213452',
    type_bus: 'super patas 32 seat',
    date: '12-02-2000',
    driver: 'supriady',
    hour: '15:00',
    payment: '2.000.000',
    route: 'banda aceh - medan',
    status: 'berangkat',
    vehicle: 'patas'
}));

export const dataPassenger: TDataPassenger[] = [...new Array(4)].map((el, i) => ({
    key: i,
    id: i,
    booking_code: 'FASDF2345',
    from: 'medan',
    to: 'aceh',
    name: 'rumini',
    phone: '+62234523',
    price: '1000000',
    remaining_payment: '4000',
    repayment: 'LUNAS',
    schema_payment: 'DP',
    seat: '1,2,3',
    station_booking: 'aceh'
}));

export const dataExpedition: TDataExpedition[] = [...new Array(3)].map((el, i) => ({
    key: i,
    id: i,
    receipt_number: '1231231',
    sender_name: 'daniel',
    sender_phone: '+65223452',
    receiver_name: 'ijah',
    receiver_phone: '+624234234',
    destination: 'aceh',
    item_name: 'beras',
    price: '120000',
    station_booking: 'medan',
}));

export const dataTempPassenger: TDataTemporaryPassenger[] = [...new Array(1)].map((el, i) => ({
    key: i,
    id: i,
    from: 'sigli',
    to: 'medan',
    total_passenger: 3,
    price: '1000000'
}));

export const dummyRoute = [
    {
        label: "Banda Aceh - medan",
        value: 1,
    },
    {
        label: "Medan - banda aceh",
        value: 2,
    }
]
export const dummyStatus = [
    {
        label: "status 1",
        value: 1,
    },
    {
        label: "status 2",
        value: 2,
    }
]