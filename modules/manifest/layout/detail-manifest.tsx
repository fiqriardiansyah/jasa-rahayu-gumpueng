import { LeftOutlined } from '@ant-design/icons';
import { Button, notification } from 'antd';
import type { NotificationPlacement } from 'antd/es/notification';
import { useRef, useState } from 'react';
import { useSetRecoilState } from 'recoil';

import { indexSelector } from 'states';

import DetailManifestHeader from '../components/detail-header';
import { FDataRepayment, TDataExpedition, TDataPassenger } from '../models';

import ModalFormTempPassenger from '../components/modal-form-temp-passenger';
import ModalRepayment from '../components/modal-repayment';

// table
import ExpeditionTable from '../components/expedition-table';
import PassengerTable from '../components/passenger-table';
import TempPassengerTable from '../components/temp-passenger-table';

const DetailManifest = () => {
  const indexState = useSetRecoilState(indexSelector);

  const [notif, notifContextHolder] = notification.useNotification();

  const [loading, setLoading] = useState({
    repayment: false,
  });
  const repaymentTrigger = useRef<HTMLButtonElement | null>(null);
  const addTempPassengerTrigger = useRef<HTMLButtonElement | null>(null);

  const onSubmitRepayment = (data: FDataRepayment, callback: () => void) => {
    setLoading((prev) => ({ ...prev, repayment: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil disimpan!'
      );
      setLoading((prev) => ({ ...prev, repayment: false }));
      callback();
    }, 2000);
  };

  const onClickPrintTicket = (data: TDataPassenger) => {
    console.log(data);
  };

  const onClickPrintReceipt = (data: TDataExpedition) => {
    console.log(data);
  };

  const onClickRemainingPayment = (data: TDataPassenger) => {
    if (repaymentTrigger.current) {
      repaymentTrigger.current.dataset.passenger = JSON.stringify(data);
      repaymentTrigger.current.click();
    }
  };

  const onClickAddTempPassenger = () => {
    if (addTempPassengerTrigger.current) {
      addTempPassengerTrigger.current.click();
    }
  };

  const openNotificationSuccess = (
    placement: NotificationPlacement,
    title: string,
    description: string
  ) => {
    notif.success({
      message: title,
      description: description,
      placement,
    });
  };

  return (
    <>
      {notifContextHolder}
      <ModalRepayment loading={loading.repayment} onSubmit={onSubmitRepayment}>
        {(data) => (
          <button
            ref={repaymentTrigger}
            onClick={() =>
              data.openModalWithData(
                repaymentTrigger.current?.dataset.passenger
              )
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalRepayment>
      <ModalFormTempPassenger loading={false}>
        {(data) => (
          <button
            ref={addTempPassengerTrigger}
            onClick={data.openModal}
            className="w-0 h-0 hidden"
          />
        )}
      </ModalFormTempPassenger>
      <div className="w-full pb-20">
        <Button
          type="primary"
          className="mb-5"
          icon={<LeftOutlined />}
          onClick={() => {
            indexState((prev) => ({
              ...prev,
              stepManifest: 0,
            }));
          }}
        >
          Kembali
        </Button>
        <DetailManifestHeader />
        <p className="capitalize text-lg font-bold mt-10">
          Data pemesanan penumpang
        </p>
        <PassengerTable
          onClickPrintTicket={onClickPrintTicket}
          onClickRemainingPayment={onClickRemainingPayment}
        />
        <p className="capitalize text-lg font-bold mt-10">
          Data pemesanan ekspedisi
        </p>
        <ExpeditionTable onClickPrintReceipt={onClickPrintReceipt} />
        <div className="flex items-center justify-between">
          <p className="capitalize text-lg font-bold mt-10">
            Data pemesanan penumpang sementara
          </p>
          <Button type="default" onClick={onClickAddTempPassenger}>
            + Tambah penumpang sementara
          </Button>
        </div>
        <TempPassengerTable />
      </div>
    </>
  );
};

export default DetailManifest;
