import { useSetRecoilState } from 'recoil';
import FormFilter from '../components/form-filter';
import ManifestTable from '../components/manifest-table';
import { TDataManifest } from '../models';

import { indexSelector } from 'states';

const Manifests = () => {
  const indexState = useSetRecoilState(indexSelector);

  const onClickDetail = (data: TDataManifest) => {
    indexState((prev) => ({
      ...prev,
      stepManifest: 1,
    }));
  };

  return (
    <div className="w-full">
      <FormFilter />
      <ManifestTable onClickDetail={onClickDetail} />
    </div>
  );
};

export default Manifests;
