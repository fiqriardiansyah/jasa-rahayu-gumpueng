export type FDataFilter = {
    id_manifest: string;
    driver_name: string;
    vehicle: string;
    route: string;
    date: string;
    status: string;
}

export type FDataRepayment = {
    name: string;
    date: string;
}

export type BaseTable = {
    key: string | number;
    id: string | number;
}

export type TDataManifest = BaseTable & {
    type_bus: string;
    route: string;
    date: string;
    hour: string;
    status: string;
    payment: string;
    vehicle: string;
    driver: string;
}

export type TDataPassenger = BaseTable & {
    booking_code: string;
    name: string;
    phone: string;
    seat: string;
    from: string;
    to: string;
    price: string;
    remaining_payment: string;
    schema_payment: string;
    station_booking: string;
    repayment: string;
}

export type TDataExpedition = BaseTable & {
    receipt_number: string;
    sender_name: string;
    sender_phone: string;
    receiver_name: string;
    receiver_phone: string;
    item_name: string;
    destination: string;
    price: string;
    station_booking: string;
}

export type TDataTemporaryPassenger = BaseTable & {
    from: string;
    to: string;
    total_passenger: number;
    price: string;
}