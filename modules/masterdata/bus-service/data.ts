import { TDataBusService } from './models';

export const data: TDataBusService[] = [...new Array(20)].map((el, i) => ({
    key: i,
    id: i,
    type_bus: 'super patas 32 seat'
}));