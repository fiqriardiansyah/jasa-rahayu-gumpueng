import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form, Modal, Row, Space } from 'antd';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { DataBusService, TDataBusService } from './models';

// components
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  openModalWithData: (data: string | undefined) => void;
  closeModal: () => void;
};

type Props = {
  loading: boolean;
  onSubmit: (data: DataBusService, callback: () => void) => void;
  children: (data: ChildrenProps) => void;
};

const schema: yup.SchemaOf<DataBusService> = yup.object().shape({
  type_bus: yup.string().required('Jenis bus wajib diisi'),
});

const ModalEditBusService = ({ onSubmit, loading, children }: Props) => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loadingGetRoute, setLoadingGetRoute] = useState(false); // nanti diganti dengan loading react-query
  const {
    handleSubmit,
    control,
    setValue,
    formState: { isValid },
  } = useForm<DataBusService>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    onSubmit(data, () => {
      closeModal();
    });
  });

  const closeModal = () => {
    if (loading) return;
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const openModalWithData = (data: string | undefined) => {
    if (data) {
      const busService = JSON.parse(data) as TDataBusService;
      setIsModalOpen(true);
      setLoadingGetRoute(true);
      setTimeout(() => {
        setLoadingGetRoute(false);
        form.setFieldsValue({
          type_bus: 'super patas 20 seat',
        });
        setValue('type_bus', 'super patas 20 seat');
      }, 2000);
    }
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    openModalWithData,
    closeModal,
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title={`${
          loadingGetRoute ? 'Mengambil data layanan bus...' : 'Edit layanan bus'
        }`}
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Form
          form={form}
          labelCol={{ span: 3 }}
          labelAlign="left"
          disabled={loading || loadingGetRoute}
          colon={false}
          style={{ width: '100%' }}
          onFinish={onSubmitHandler}
          layout="vertical"
        >
          <Space direction="vertical" className="w-full">
            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="type_bus"
              label="Jenis bus"
              placeholder="Jenis bus"
            />

            <Row justify="start">
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading}
                  disabled={!isValid}
                >
                  Ubah
                </Button>
                <Button type="primary" danger onClick={closeModal}>
                  Batalkan
                </Button>
              </Space>
            </Row>
          </Space>
        </Form>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalEditBusService;
