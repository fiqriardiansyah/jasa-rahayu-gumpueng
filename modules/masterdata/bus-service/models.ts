export type TDataBusService = {
    key: string | number;
    id: string | number;
    type_bus: string;
}

export type DataBusService = {
    type_bus: string;
}