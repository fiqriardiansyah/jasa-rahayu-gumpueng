import { Button, Space, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { TDataBusService } from './models';

import { data } from './data';

type Props = {
  onClickEdit: (data: TDataBusService) => void;
  onClickDelete: (data: TDataBusService) => void;
  onClickDetail: (data: TDataBusService) => void;
};

const BusServiceTable = ({
  onClickDelete,
  onClickEdit,
  onClickDetail,
}: Props) => {
  const columns: ColumnsType<TDataBusService> = [
    {
      title: 'ID',
      dataIndex: 'id',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Jenis Bus',
      dataIndex: 'type_bus',
      render: (text, record) => (
        <Button
          onClick={() => onClickDetail(record)}
          className="capitalize"
          type="link"
        >
          {text}
        </Button>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle" direction="horizontal">
          <Button type="text" onClick={() => onClickEdit(record)}>
            Edit
          </Button>
          <Button type="primary" danger onClick={() => onClickDelete(record)}>
            Hapus
          </Button>
        </Space>
      ),
    },
  ];

  return <Table columns={columns} dataSource={data} className="w-full" />;
};

export default BusServiceTable;
