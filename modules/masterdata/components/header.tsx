type Props = {
  title: string;
  action: React.ReactNode;
};

const Header = ({ title, action }: Props) => {
  return (
    <div className="flex items-center justify-between w-full mb-10 sticky top-0 left-0 z-10 bg-white shadow container mx-auto px-5 py-2">
      <h1 className="capitalize text-gray-600">{`Master Data - ${title}`}</h1>
      <div className="">{action}</div>
    </div>
  );
};

export default Header;
