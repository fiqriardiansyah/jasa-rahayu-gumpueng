import { TDataCrew } from './models';

export const data: TDataCrew[] = [...new Array(20)].map((el, i) => ({
    key: i,
    id: i,
    name: 'cacing besar alaska',
    status: 'sopir',
    phone: '02302323',
    address: 'rock bottom',
    ktp: '12322343545',
    npwp: 'safd23fqw34r234'
}));