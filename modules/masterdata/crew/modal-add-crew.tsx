import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form, Modal, Row, Space } from 'antd';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { DataCrew } from './models';

// components
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  closeModal: () => void;
};

type Props = {
  onSubmit: (data: DataCrew, callback: () => void) => void;
  loading: boolean;
  children: (data: ChildrenProps) => void;
};

const schema: yup.SchemaOf<Partial<DataCrew>> = yup.object().shape({
  name: yup.string().required('Nama kendaraan wajib diisi'),
  address: yup.string().required('Alamat wajib diisi'),
  status: yup.string().required('Status wajib diisi'),
  phone: yup.string().required('No handphone wajib diisi'),
  ktp: yup.string().required('Ktp wajib diisi'),
  npwp: yup.string().required('Npwp wajib diisi'),
});

const ModalAddCrew = ({ onSubmit, loading, children }: Props) => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const {
    handleSubmit,
    control,
    formState: { isValid },
  } = useForm<DataCrew>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    onSubmit(data, () => {
      closeModal();
    });
  });

  const closeModal = () => {
    if (loading) return;
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    closeModal,
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title="Tambah Kru"
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Form
          form={form}
          labelCol={{ span: 3 }}
          labelAlign="left"
          disabled={loading}
          colon={false}
          style={{ width: '100%' }}
          onFinish={onSubmitHandler}
          layout="vertical"
        >
          <Space direction="vertical" className="w-full">
            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="name"
              label="Nama"
              placeholder="Nama"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="status"
              label="Status"
              placeholder="status"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="address"
              label="Alamat"
              placeholder="Alamat"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="phone"
              label="No Handphone"
              placeholder="No handphone"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="ktp"
              label="Ktp"
              placeholder="ktp"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="npwp"
              label="Npwp"
              placeholder="npwp"
            />

            <Row justify="start">
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading}
                  disabled={!isValid}
                >
                  Simpan
                </Button>
                <Button type="primary" danger onClick={closeModal}>
                  Batalkan
                </Button>
              </Space>
            </Row>
          </Space>
        </Form>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalAddCrew;
