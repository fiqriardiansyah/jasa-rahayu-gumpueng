export type TDataCrew = {
    key: string | number;
    id: string | number;
    name: string;
    status: string;
    phone: string;
    address: string;
    ktp: string;
    npwp: string;
}

export type DataCrew = Omit<TDataCrew, 'key' | 'id'>;