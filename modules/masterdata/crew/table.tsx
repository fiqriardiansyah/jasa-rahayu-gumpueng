import { Button, Space, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { TDataCrew } from './models';

import { data } from './data';

type Props = {
  onClickEdit: (data: TDataCrew) => void;
  onClickDelete: (data: TDataCrew) => void;
  onClickDetail: (data: TDataCrew) => void;
};

const CrewTable = ({ onClickDelete, onClickEdit, onClickDetail }: Props) => {
  const columns: ColumnsType<TDataCrew> = [
    {
      title: 'ID',
      dataIndex: 'id',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Nama',
      dataIndex: 'name',
      render: (text, record) => (
        <Button
          onClick={() => onClickDetail(record)}
          className="capitalize"
          type="link"
        >
          {text}
        </Button>
      ),
    },
    {
      title: 'Status',
      dataIndex: 'status',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'No Handphone',
      dataIndex: 'phone',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Alamat',
      dataIndex: 'address',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Ktp',
      dataIndex: 'ktp',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Npwp',
      dataIndex: 'npwp',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle" direction="horizontal">
          <Button type="text" onClick={() => onClickEdit(record)}>
            Edit
          </Button>
          <Button type="primary" danger onClick={() => onClickDelete(record)}>
            Hapus
          </Button>
        </Space>
      ),
    },
  ];

  return <Table columns={columns} dataSource={data} className="w-full" />;
};

export default CrewTable;
