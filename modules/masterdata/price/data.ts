import { TDataPrice } from './models';

export const data: TDataPrice[] = [...new Array(20)].map((el, i) => ({
    key: i,
    id: i,
    type_bus: 'minibus/hiace',
    city_dest: 'asgard',
    city_from: 'aceh',
    price: '100000'
}));

export const dataCategoryId = [
    {
        label: "Super patas 34",
        value: 1,
    },
    {
        label: "Minibus/hiace",
        value: 2,
    }
]