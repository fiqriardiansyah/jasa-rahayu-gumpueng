import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form, Modal, Row, Space } from 'antd';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { DataPrice } from './models';

import { dataCategoryId } from './data';

// components
import ControlledInputNumber from 'components/form/controlled-inputs/controlled-input-number';
import ControlledInputSelect from 'components/form/controlled-inputs/controlled-input-select';
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  closeModal: () => void;
};

type Props = {
  onSubmit: (data: DataPrice, callback: () => void) => void;
  loading: boolean;
  children: (data: ChildrenProps) => void;
};

const schema: yup.SchemaOf<DataPrice> = yup.object().shape({
  city_dest: yup.string().required('Kota tujuan wajib diisi'),
  city_from: yup.string().required('Kota awal wajib diisi'),
  type_bus: yup.string().required('Jenis bus wajib diisi'),
  price: yup.string().required('Harga wajib diisi'),
});

const ModalAddPrice = ({ onSubmit, loading, children }: Props) => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const {
    handleSubmit,
    control,
    formState: { isValid },
  } = useForm<DataPrice>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    onSubmit(data, () => {
      closeModal();
    });
  });

  const closeModal = () => {
    if (loading) return;
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    closeModal,
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title="Tambah Harga"
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Form
          form={form}
          labelCol={{ span: 3 }}
          labelAlign="left"
          disabled={loading}
          colon={false}
          style={{ width: '100%' }}
          onFinish={onSubmitHandler}
          layout="vertical"
        >
          <Space direction="vertical" className="w-full">
            <ControlledInputNumber
              control={control}
              labelCol={{ xs: 12 }}
              name="price"
              label="Harga"
              placeholder="Harga"
            />

            <ControlledInputSelect
              showSearch
              name="type_bus"
              label="Jenis bus"
              placeholder="Jenis bus"
              optionFilterProp="children"
              control={control}
              options={dataCategoryId}
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="city_from"
              label="Kota asal trayek"
              placeholder="Kota asal"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="city_dest"
              label="Kota tujuan trayek"
              placeholder="Kota tujuan"
            />

            <Row justify="start">
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading}
                  disabled={!isValid}
                >
                  Simpan
                </Button>
                <Button type="primary" danger onClick={closeModal}>
                  Batalkan
                </Button>
              </Space>
            </Row>
          </Space>
        </Form>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalAddPrice;
