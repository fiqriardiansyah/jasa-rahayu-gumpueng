import { Descriptions, Modal } from 'antd';
import { useState } from 'react';
import Utils from 'utils';

import { TDataPrice } from './models';

// components

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  openModalWithData: (data: string | undefined) => void;
  closeModal: () => void;
};

type Props = {
  children: (data: ChildrenProps) => void;
};

const ModalDetailPrice = ({ children }: Props) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [data, setData] = useState<null | TDataPrice>(null);

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const openModalWithData = (data: string | undefined) => {
    if (data) {
      const parse = JSON.parse(data) as TDataPrice;
      setData(parse);
      openModal();
    }
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    openModalWithData,
    closeModal,
  };

  return (
    <>
      <Modal
        title="Detail"
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Descriptions
          title={`${data?.type_bus} - ${data?.price}`}
          column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
          colon={false}
          labelStyle={{ color: '#919EAB', width: 200 }}
          bordered
        >
          <Descriptions.Item label="ID">{data?.id}</Descriptions.Item>
          <Descriptions.Item label="Jenis bus">
            {data?.type_bus}
          </Descriptions.Item>
          <Descriptions.Item label="Kota asal">
            {data?.city_from}
          </Descriptions.Item>
          <Descriptions.Item label="Kota tujuan">
            {data?.city_dest}
          </Descriptions.Item>
          <Descriptions.Item label="Harga">
            {Utils.convertToStringFormat(data?.price as unknown as number)}
          </Descriptions.Item>
        </Descriptions>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalDetailPrice;
