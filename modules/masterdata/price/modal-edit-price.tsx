import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form, Modal, Row, Space } from 'antd';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { DataPrice, TDataPrice } from './models';

import { dataCategoryId } from './data';

// components
import ControlledInputNumber from 'components/form/controlled-inputs/controlled-input-number';
import ControlledInputSelect from 'components/form/controlled-inputs/controlled-input-select';
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  openModalWithData: (data: string | undefined) => void;
  closeModal: () => void;
};

type Props = {
  loading: boolean;
  onSubmit: (data: DataPrice, callback: () => void) => void;
  children: (data: ChildrenProps) => void;
};

const schema: yup.SchemaOf<DataPrice> = yup.object().shape({
  city_dest: yup.string().required('Kota tujuan wajib diisi'),
  city_from: yup.string().required('Kota awal wajib diisi'),
  type_bus: yup.string().required('Jenis bus wajib diisi'),
  price: yup.string().required('Harga wajib diisi'),
});

const ModalEditPrice = ({ onSubmit, loading, children }: Props) => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loadingGetRoute, setLoadingGetRoute] = useState(false); // nanti diganti dengan loading react-query
  const {
    handleSubmit,
    control,
    setValue,
    formState: { isValid },
  } = useForm<DataPrice>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    onSubmit(data, () => {
      closeModal();
    });
  });

  const closeModal = () => {
    if (loading) return;
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const openModalWithData = (data: string | undefined) => {
    if (data) {
      const price = JSON.parse(data) as TDataPrice;
      setIsModalOpen(true);
      setLoadingGetRoute(true);
      setTimeout(() => {
        setLoadingGetRoute(false);
        form.setFieldsValue({
          city_from: 'bikini bottom',
          city_dest: 'asgard',
          type_bus: 'super patas 100 seat',
          price: '100.000',
        });
        setValue('type_bus', 'super patas 100 seat');
        setValue('city_from', 'bikini bottom');
        setValue('city_dest', 'asgard');
        setValue('price', '100.000');
      }, 2000);
    }
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    openModalWithData,
    closeModal,
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title={`${loadingGetRoute ? 'Mengambil data harga...' : 'Edit Harga'}`}
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Form
          form={form}
          labelCol={{ span: 3 }}
          labelAlign="left"
          disabled={loading || loadingGetRoute}
          colon={false}
          style={{ width: '100%' }}
          onFinish={onSubmitHandler}
          layout="vertical"
        >
          <Space direction="vertical" className="w-full">
            <ControlledInputNumber
              control={control}
              labelCol={{ xs: 12 }}
              name="price"
              label="Harga"
              placeholder="Harga"
            />

            <ControlledInputSelect
              showSearch
              name="type_bus"
              label="Jenis bus"
              placeholder="Jenis bus"
              optionFilterProp="children"
              control={control}
              options={dataCategoryId}
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="city_from"
              label="Kota asal trayek"
              placeholder="Kota asal"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="city_dest"
              label="Kota tujuan trayek"
              placeholder="Kota tujuan"
            />

            <Row justify="start">
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading}
                  disabled={!isValid}
                >
                  Ubah
                </Button>
                <Button type="primary" danger onClick={closeModal}>
                  Batalkan
                </Button>
              </Space>
            </Row>
          </Space>
        </Form>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalEditPrice;
