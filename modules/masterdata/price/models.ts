export type TDataPrice = {
    key: string | number;
    id: string | number;
    type_bus: string;
    city_from: string;
    city_dest: string;
    price: string;
}

export type DataPrice = Omit<TDataPrice, 'key' | 'id'>;