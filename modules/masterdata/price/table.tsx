import { Button, Space, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { TDataPrice } from './models';

import Utils from 'utils';
import { data } from './data';

type Props = {
  onClickEdit: (data: TDataPrice) => void;
  onClickDelete: (data: TDataPrice) => void;
  onClickDetail: (data: TDataPrice) => void;
};

const PriceTable = ({ onClickDelete, onClickEdit, onClickDetail }: Props) => {
  const columns: ColumnsType<TDataPrice> = [
    {
      title: 'ID',
      dataIndex: 'id',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Jenis Bus',
      dataIndex: 'type_bus',
      render: (text, record) => (
        <Button
          type="link"
          onClick={() => onClickDetail(record)}
          className="capitalize"
        >
          {text}
        </Button>
      ),
    },
    {
      title: 'Kota asal',
      dataIndex: 'city_from',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Kota tujuan',
      dataIndex: 'city_dest',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Harga',
      dataIndex: 'price',
      render: (text) => (
        <p className="capitalize">{Utils.convertToStringFormat(text)}</p>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle" direction="horizontal">
          <Button type="text" onClick={() => onClickEdit(record)}>
            Edit
          </Button>
          <Button type="primary" danger onClick={() => onClickDelete(record)}>
            Hapus
          </Button>
        </Space>
      ),
    },
  ];

  return <Table columns={columns} dataSource={data} className="w-full" />;
};

export default PriceTable;
