import { TDataRoute } from './models';

export const data: TDataRoute[] = [...new Array(20)].map((el, i) => ({
    key: i,
    id: i,
    hour: '17:00',
    route_name: 'medan - asgard',
    type_bus: 'minibus/hiace'
}));

export const dataCategoryId = [
    {
        label: "Super patas 34",
        value: 1,
    },
    {
        label: "Minibus/hiace",
        value: 2,
    }
]