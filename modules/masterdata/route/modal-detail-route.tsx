import { Descriptions, Modal } from 'antd';
import { useState } from 'react';

import { TDataRoute } from './models';

// components

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  openModalWithData: (data: string | undefined) => void;
  closeModal: () => void;
};

type Props = {
  children: (data: ChildrenProps) => void;
};

const ModalDetailRoute = ({ children }: Props) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [data, setData] = useState<null | TDataRoute>(null);

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const openModalWithData = (data: string | undefined) => {
    if (data) {
      const parse = JSON.parse(data) as TDataRoute;
      setData(parse);
      openModal();
    }
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    openModalWithData,
    closeModal,
  };

  return (
    <>
      <Modal
        title="Detail"
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Descriptions
          title={`${data?.route_name} - ${data?.type_bus}`}
          column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
          colon={false}
          labelStyle={{ color: '#919EAB', width: 200 }}
          bordered
        >
          <Descriptions.Item label="ID">{data?.id}</Descriptions.Item>
          <Descriptions.Item label="Jenis bus">
            {data?.type_bus}
          </Descriptions.Item>
          <Descriptions.Item label="Nama trayek">
            {data?.route_name}
          </Descriptions.Item>
          <Descriptions.Item label="Jam">{data?.hour}</Descriptions.Item>
        </Descriptions>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalDetailRoute;
