import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form, Modal, Row, Space } from 'antd';
import moment from 'moment';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { DataRoute, TDataRoute } from './models';

import { dataCategoryId } from './data';

// components
import ControlledInputSelect from 'components/form/controlled-inputs/controlled-input-select';
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';
import ControlledInputTime from 'components/form/controlled-inputs/controlled-input-time';

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  openModalWithData: (data: string | undefined) => void;
  closeModal: () => void;
};

type Props = {
  loading: boolean;
  onSubmit: (data: DataRoute, callback: () => void) => void;
  children: (data: ChildrenProps) => void;
};

const schema: yup.SchemaOf<DataRoute> = yup.object().shape({
  hour: yup.string().required('Jam keberangkatan wajib diisi'),
  category_id: yup.number().required('Kategori id wajib diisi'),
  city_dest: yup.string().required('Kota tujuan wajib diisi'),
  city_from: yup.string().required('Kota awal wajib diisi'),
});

const ModalEditRoute = ({ onSubmit, loading, children }: Props) => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loadingGetRoute, setLoadingGetRoute] = useState(false); // nanti diganti dengan loading react-query
  const {
    handleSubmit,
    control,
    setValue,
    formState: { isValid },
  } = useForm<DataRoute>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    onSubmit(
      {
        ...data,
        hour: moment(data.hour).format('HH:mm'),
      },
      () => {
        closeModal();
      }
    );
  });

  const closeModal = () => {
    if (loading) return;
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const openModalWithData = (data: string | undefined) => {
    if (data) {
      const route = JSON.parse(data) as TDataRoute;
      setIsModalOpen(true);
      setLoadingGetRoute(true);
      setTimeout(() => {
        setLoadingGetRoute(false);
        form.setFieldsValue({
          category_id: 1,
          city_from: 'bikini bottom',
          city_dest: 'asgard',
          hour: moment(route.hour, 'HH:mm'),
        });
        setValue('category_id', 1);
        setValue('city_from', 'bikini bottom');
        setValue('city_dest', 'asgard');
        setValue('hour', moment(route.hour, 'HH:mm').toString());
      }, 2000);
    }
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    openModalWithData,
    closeModal,
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title={`${
          loadingGetRoute ? 'Mengambil data trayek...' : 'Edit Trayek'
        }`}
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Form
          form={form}
          labelCol={{ span: 3 }}
          labelAlign="left"
          disabled={loading || loadingGetRoute}
          colon={false}
          style={{ width: '100%' }}
          onFinish={onSubmitHandler}
          layout="vertical"
        >
          <Space direction="vertical" className="w-full">
            <ControlledInputTime
              control={control}
              name="hour"
              label="Jam keberangkatan"
              labelCol={{ xs: 12 }}
            />

            <ControlledInputSelect
              showSearch
              name="category_id"
              label="Kategori id"
              placeholder="Kategori id"
              optionFilterProp="children"
              control={control}
              options={dataCategoryId}
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="city_from"
              label="Kota asal trayek"
              placeholder="Kota asal"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="city_dest"
              label="Kota tujuan trayek"
              placeholder="Kota tujuan"
            />

            <Row justify="start">
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading}
                  disabled={!isValid}
                >
                  Ubah
                </Button>
                <Button type="primary" danger onClick={closeModal}>
                  Batalkan
                </Button>
              </Space>
            </Row>
          </Space>
        </Form>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalEditRoute;
