export type TDataRoute = {
    key: string | number;
    id: string | number;
    type_bus: string;
    route_name: string;
    hour: string;
}

export type DataRoute = {
    category_id: number;
    hour: string;
    city_from: string;
    city_dest: string;
}