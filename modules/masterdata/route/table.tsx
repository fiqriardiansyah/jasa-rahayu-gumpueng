import { Button, Space, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { TDataRoute } from './models';

import { data } from './data';

type Props = {
  onClickEdit: (data: TDataRoute) => void;
  onClickDelete: (data: TDataRoute) => void;
  onClickDetail: (data: TDataRoute) => void;
};

const RouteTable = ({ onClickDelete, onClickEdit, onClickDetail }: Props) => {
  const columns: ColumnsType<TDataRoute> = [
    {
      title: 'ID',
      dataIndex: 'id',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Jenis Bus',
      dataIndex: 'type_bus',
      render: (text, record) => (
        <Button
          type="link"
          onClick={() => onClickDetail(record)}
          className="capitalize"
        >
          {text}
        </Button>
      ),
    },
    {
      title: 'Nama Trayek',
      dataIndex: 'route_name',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Jam',
      dataIndex: 'hour',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle" direction="horizontal">
          <Button type="text" onClick={() => onClickEdit(record)}>
            Edit
          </Button>
          <Button type="primary" danger onClick={() => onClickDelete(record)}>
            Hapus
          </Button>
        </Space>
      ),
    },
  ];

  return <Table columns={columns} dataSource={data} className="w-full" />;
};

export default RouteTable;
