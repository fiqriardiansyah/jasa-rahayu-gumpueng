import { TDataStation } from './models';

export const data: TDataStation[] = [...new Array(20)].map((el, i) => ({
    key: i,
    id: i,
    name: 'bikini botttom',
    address: 'jl.kerang',
    email: 'mr.krab@gmail.com',
    phone: '123123123',
    responsible_person: 'tuan krab'
}));