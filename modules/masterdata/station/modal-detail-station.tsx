import { Descriptions, Modal } from 'antd';
import { useState } from 'react';

import { TDataStation } from './models';

// components

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  openModalWithData: (data: string | undefined) => void;
  closeModal: () => void;
};

type Props = {
  children: (data: ChildrenProps) => void;
};

const ModalDetailStation = ({ children }: Props) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [data, setData] = useState<null | TDataStation>(null);

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const openModalWithData = (data: string | undefined) => {
    if (data) {
      const parse = JSON.parse(data) as TDataStation;
      setData(parse);
      openModal();
    }
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    openModalWithData,
    closeModal,
  };

  return (
    <>
      <Modal
        title="Detail"
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Descriptions
          title={data?.name}
          column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
          colon={false}
          labelStyle={{ color: '#919EAB', width: 200 }}
          bordered
        >
          <Descriptions.Item label="ID">{data?.id}</Descriptions.Item>
          <Descriptions.Item label="Nama loket">{data?.name}</Descriptions.Item>
          <Descriptions.Item label="Nama penanggung jawab">
            {data?.responsible_person}
          </Descriptions.Item>
          <Descriptions.Item label="Email">{data?.email}</Descriptions.Item>
          <Descriptions.Item label="No handphone">
            {data?.phone}
          </Descriptions.Item>
          <Descriptions.Item label="Alamat">{data?.address}</Descriptions.Item>
        </Descriptions>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalDetailStation;
