import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form, Modal, Row, Space } from 'antd';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { DataStation, TDataStation } from './models';

// components
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  openModalWithData: (data: string | undefined) => void;
  closeModal: () => void;
};

type Props = {
  loading: boolean;
  onSubmit: (data: DataStation, callback: () => void) => void;
  children: (data: ChildrenProps) => void;
};

const schema: yup.SchemaOf<DataStation> = yup.object().shape({
  name: yup.string().required('Nama loket wajib diisi'),
  responsible_person: yup
    .string()
    .required('Nama penanggung jawab wajib diisi'),
  address: yup.string().required('Alamat wajib diisi'),
  email: yup.string().email().required('Email wajib diisi'),
  phone: yup.string().required('No handphone diisi'),
});

const ModalEditStation = ({ onSubmit, loading, children }: Props) => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loadingGetRoute, setLoadingGetRoute] = useState(false); // nanti diganti dengan loading react-query
  const {
    handleSubmit,
    control,
    setValue,
    formState: { isValid },
  } = useForm<DataStation>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    onSubmit(data, () => {
      closeModal();
    });
  });

  const closeModal = () => {
    if (loading) return;
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const openModalWithData = (data: string | undefined) => {
    if (data) {
      const station = JSON.parse(data) as TDataStation;
      setIsModalOpen(true);
      setLoadingGetRoute(true);
      setTimeout(() => {
        setLoadingGetRoute(false);
        form.setFieldsValue({
          name: 'bikini bottom',
          responsible_person: 'spongebob',
          email: 'sponge@gmail.com',
          phone: '2000230230',
          address: 'jl.rajungan',
        });
        setValue('name', 'bikini bottom');
        setValue('responsible_person', 'spongebob');
        setValue('email', 'sponge@gmail.com');
        setValue('phone', '2000230230');
        setValue('address', 'jl.rajungan');
      }, 2000);
    }
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    openModalWithData,
    closeModal,
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title={`${loadingGetRoute ? 'Mengambil data loket...' : 'Edit Loket'}`}
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Form
          form={form}
          labelCol={{ span: 3 }}
          labelAlign="left"
          disabled={loading || loadingGetRoute}
          colon={false}
          style={{ width: '100%' }}
          onFinish={onSubmitHandler}
          layout="vertical"
        >
          <Space direction="vertical" className="w-full">
            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="name"
              label="Nama Loket"
              placeholder="Nama loket"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="responsible_person"
              label="Nama Penanggung Jawab"
              placeholder="Nama penanggung jawab"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="address"
              label="Alamat"
              placeholder="Alamat"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="phone"
              label="No Handphone"
              placeholder="No handphone"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="email"
              label="Email"
              placeholder="Email"
            />

            <Row justify="start">
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading}
                  disabled={!isValid}
                >
                  Ubah
                </Button>
                <Button type="primary" danger onClick={closeModal}>
                  Batalkan
                </Button>
              </Space>
            </Row>
          </Space>
        </Form>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalEditStation;
