export type TDataStation = {
    key: string | number;
    id: string | number;
    name: string;
    responsible_person: string;
    email: string;
    phone: string;
    address: string;
}

export type DataStation = Omit<TDataStation, 'key' | 'id'>;