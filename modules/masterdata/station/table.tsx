import { Button, Space, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { TDataStation } from './models';

import { data } from './data';

type Props = {
  onClickEdit: (data: TDataStation) => void;
  onClickDelete: (data: TDataStation) => void;
  onClickDetail: (data: TDataStation) => void;
};

const StationTable = ({ onClickDelete, onClickEdit, onClickDetail }: Props) => {
  const columns: ColumnsType<TDataStation> = [
    {
      title: 'ID',
      dataIndex: 'id',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Nama Loket',
      dataIndex: 'name',
      render: (text, record) => (
        <Button
          type="link"
          onClick={() => onClickDetail(record)}
          className="capitalize"
        >
          {text}
        </Button>
      ),
    },
    {
      title: 'Nama Penanggung Jawab',
      dataIndex: 'responsible_person',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Email',
      dataIndex: 'email',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'No Hp',
      dataIndex: 'phone',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Alamat',
      dataIndex: 'address',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle" direction="horizontal">
          <Button type="text" onClick={() => onClickEdit(record)}>
            Edit
          </Button>
          <Button type="primary" danger onClick={() => onClickDelete(record)}>
            Hapus
          </Button>
        </Space>
      ),
    },
  ];

  return <Table columns={columns} dataSource={data} className="w-full" />;
};

export default StationTable;
