import { TDataVehicle } from './models';

export const data: TDataVehicle[] = [...new Array(20)].map((el, i) => ({
    key: i,
    id: i,
    name: 'mercedes benz',
    plat_no: 'BL 2344',
    purchase_date: '12-feb-2300',
    year: '2000',
    acquisition_cost: '',
    estimated_value: '',
    service_life_in_years: '',
    vendor: '',
}));