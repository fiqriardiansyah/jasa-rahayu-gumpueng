import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form, Modal, Row, Space } from 'antd';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { DataVehicle } from './models';

// components
import ControlledInputDate from 'components/form/controlled-inputs/controlled-input-date';
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  closeModal: () => void;
};

type Props = {
  onSubmit: (data: DataVehicle, callback: () => void) => void;
  loading: boolean;
  children: (data: ChildrenProps) => void;
};

const schema: yup.SchemaOf<Partial<DataVehicle>> = yup.object().shape({
  name: yup.string().required('Nama kendaraan wajib diisi'),
  plat_no: yup.string().required('Nomor plat wajib diisi'),
  year: yup.string().required('Tahun kendaraan wajib diisi'),
  purchase_date: yup.string().required('Tanggal pembelian wajib diisi'),
  vendor: yup.string(),
  acquisition_cost: yup.string(),
  service_life_in_years: yup.string(),
  estimated_value: yup.string(),
});

const ModalAddVehicle = ({ onSubmit, loading, children }: Props) => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const {
    handleSubmit,
    control,
    formState: { isValid },
  } = useForm<DataVehicle>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    onSubmit(data, () => {
      closeModal();
    });
  });

  const closeModal = () => {
    if (loading) return;
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    closeModal,
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title="Tambah Kendaraan"
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Form
          form={form}
          labelCol={{ span: 3 }}
          labelAlign="left"
          disabled={loading}
          colon={false}
          style={{ width: '100%' }}
          onFinish={onSubmitHandler}
          layout="vertical"
        >
          <Space direction="vertical" className="w-full">
            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="name"
              label="Nama kendaraan"
              placeholder="Nama kendaraan"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="plat_no"
              label="Nomor plat"
              placeholder="Nomor plat"
            />

            <ControlledInputDate
              control={control}
              name="purchase_date"
              label="Tanggal pembelian"
              labelCol={{ xs: 12 }}
            />

            <ControlledInputDate
              control={control}
              name="year"
              label="Tahun kendaraan"
              picker="year"
              labelCol={{ xs: 12 }}
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="vendor"
              label="Vendor"
              placeholder="Vendor"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="acquisition_cost"
              label="Harga Perolehan"
              placeholder="Harga Perolehan (sampai unit siap pakai)"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="service_life_in_years"
              label="Masa Pakai"
              placeholder="Masa pakai dalam tahun"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="estimated_value"
              label="Estimasi Nilai Sisa"
              placeholder="Estimasi nilai sisa (setelah masa pakai)"
            />

            <Row justify="start">
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading}
                  disabled={!isValid}
                >
                  Simpan
                </Button>
                <Button type="primary" danger onClick={closeModal}>
                  Batalkan
                </Button>
              </Space>
            </Row>
          </Space>
        </Form>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalAddVehicle;
