import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form, Modal, Row, Space } from 'antd';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { DataVehicle, TDataVehicle } from './models';

// components
import ControlledInputDate from 'components/form/controlled-inputs/controlled-input-date';
import ControlledInputText from 'components/form/controlled-inputs/controlled-input-text';
import moment from 'moment';

type ChildrenProps = {
  isModalOpen: boolean;
  openModal: () => void;
  openModalWithData: (data: string | undefined) => void;
  closeModal: () => void;
};

type Props = {
  loading: boolean;
  onSubmit: (data: DataVehicle, callback: () => void) => void;
  children: (data: ChildrenProps) => void;
};

const schema: yup.SchemaOf<Partial<DataVehicle>> = yup.object().shape({
  name: yup.string().required('Nama kendaraan wajib diisi'),
  plat_no: yup.string().required('Nomor plat wajib diisi'),
  year: yup.string().required('Tahun kendaraan wajib diisi'),
  purchase_date: yup.string().required('Tanggal pembelian wajib diisi'),
  vendor: yup.string(),
  acquisition_cost: yup.string(),
  service_life_in_years: yup.string(),
  estimated_value: yup.string(),
});

const ModalEditVehicle = ({ onSubmit, loading, children }: Props) => {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loadingGetRoute, setLoadingGetRoute] = useState(false); // nanti diganti dengan loading react-query
  const {
    handleSubmit,
    control,
    setValue,
    formState: { isValid },
  } = useForm<DataVehicle>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = handleSubmit((data) => {
    onSubmit(data, () => {
      closeModal();
    });
  });

  const closeModal = () => {
    if (loading) return;
    setIsModalOpen(false);
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const openModalWithData = (data: string | undefined) => {
    if (data) {
      const route = JSON.parse(data) as TDataVehicle;
      setIsModalOpen(true);
      setLoadingGetRoute(true);
      setTimeout(() => {
        setLoadingGetRoute(false);
        form.setFieldsValue({
          name: 'kijang',
          plat_no: '123abc',
          year: moment(moment.now()),
          purchase_date: moment(moment.now()),
          vendor: 'testttt',
          acquisition_cost: 'testttt',
          service_life_in_years: 'testttt',
          estimated_value: 'testttt',
        });
        setValue('name', 'kijang');
        setValue('plat_no', '123abc');
        setValue('year', moment(moment.now()));
        setValue('purchase_date', moment(moment.now()));
        setValue('vendor', 'testttt');
        setValue('acquisition_cost', 'testttt');
        setValue('service_life_in_years', 'testttt');
        setValue('estimated_value', 'testttt');
      }, 2000);
    }
  };

  const childrenData: ChildrenProps = {
    isModalOpen,
    openModal,
    openModalWithData,
    closeModal,
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title={`${
          loadingGetRoute ? 'Mengambil data kendaraan...' : 'Edit Kendaraan'
        }`}
        open={isModalOpen}
        onCancel={closeModal}
        footer={null}
      >
        <Form
          form={form}
          labelCol={{ span: 3 }}
          labelAlign="left"
          disabled={loading || loadingGetRoute}
          colon={false}
          style={{ width: '100%' }}
          onFinish={onSubmitHandler}
          layout="vertical"
        >
          <Space direction="vertical" className="w-full">
            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="name"
              label="Nama kendaraan"
              placeholder="Nama kendaraan"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="plat_no"
              label="Nomor plat"
              placeholder="Nomor plat"
            />

            <ControlledInputDate
              control={control}
              name="purchase_date"
              label="Tanggal pembelian"
              labelCol={{ xs: 12 }}
            />

            <ControlledInputDate
              control={control}
              name="year"
              picker="year"
              label="Tahun kendaraan"
              labelCol={{ xs: 12 }}
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="vendor"
              label="Vendor"
              placeholder="Vendor"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="acquisition_cost"
              label="Harga Perolehan"
              placeholder="Harga Perolehan (sampai unit siap pakai)"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="service_life_in_years"
              label="Masa Pakai"
              placeholder="Masa pakai dalam tahun"
            />

            <ControlledInputText
              control={control}
              labelCol={{ xs: 12 }}
              name="estimated_value"
              label="Estimasi Nilai Sisa"
              placeholder="Estimasi nilai sisa (setelah masa pakai)"
            />

            <Row justify="start">
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading}
                  disabled={!isValid}
                >
                  Ubah
                </Button>
                <Button type="primary" danger onClick={closeModal}>
                  Batalkan
                </Button>
              </Space>
            </Row>
          </Space>
        </Form>
      </Modal>
      {children(childrenData)}
    </>
  );
};

export default ModalEditVehicle;
