export type TDataVehicle = {
    key: string | number;
    id: string | number;
    name: string;
    plat_no: string;
    year: any;
    purchase_date: any;
    vendor: string;
    acquisition_cost: string;
    service_life_in_years: string;
    estimated_value: string;
}

export type DataVehicle = Omit<TDataVehicle, 'key' | 'id'>;