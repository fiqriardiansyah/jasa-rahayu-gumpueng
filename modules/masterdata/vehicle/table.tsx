import { Button, Space, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { TDataVehicle } from './models';

import { data } from './data';

type Props = {
  onClickEdit: (data: TDataVehicle) => void;
  onClickDelete: (data: TDataVehicle) => void;
  onClickDetail: (data: TDataVehicle) => void;
};

const VehicleTable = ({ onClickDelete, onClickEdit, onClickDetail }: Props) => {
  const columns: ColumnsType<TDataVehicle> = [
    {
      title: 'ID',
      dataIndex: 'id',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Nama bus',
      dataIndex: 'name',
      render: (text, record) => (
        <Button
          type="link"
          onClick={() => onClickDetail(record)}
          className="capitalize"
        >
          {text}
        </Button>
      ),
    },
    {
      title: 'No Plat',
      dataIndex: 'plat_no',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Tahun',
      dataIndex: 'year',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Tanggal Beli',
      dataIndex: 'purchase_date',
      render: (text) => <p className="capitalize">{text}</p>,
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle" direction="horizontal">
          <Button type="text" onClick={() => onClickEdit(record)}>
            Edit
          </Button>
          <Button type="primary" danger onClick={() => onClickDelete(record)}>
            Hapus
          </Button>
        </Space>
      ),
    },
  ];

  return <Table columns={columns} dataSource={data} className="w-full" />;
};

export default VehicleTable;
