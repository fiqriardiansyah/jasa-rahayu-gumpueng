import {
  AuditOutlined,
  DatabaseOutlined,
  PieChartOutlined,
  ReadOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Menu } from 'antd';
import { useRouter } from 'next/router';

// utils
import Routes from 'utils/routes';

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group'
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const items: MenuProps['items'] = [
  getItem('Dashboard', 'dashboard', <PieChartOutlined />, [
    getItem('Analytic', Routes.analytic),
    getItem('Sales', Routes.sales),
  ]),

  getItem('Master Data', 'master-data', <DatabaseOutlined />, [
    getItem('Trayek', Routes.route),
    getItem('Jenis Layanan Bus', Routes.busService),
    getItem('Data Harga', Routes.price),
    getItem('Data Kendaraan', Routes.vehicle),
    getItem('Data Loket', Routes.station),
    getItem('Data Kru', Routes.crew),
  ]),
  getItem('Booking', Routes.booking, <ReadOutlined />),
  getItem('Daftar Manifest', Routes.manifest, <AuditOutlined />),
];

const Sidebar = () => {
  const route = useRouter();

  const onClick: MenuProps['onClick'] = (e) => {
    route.push(e.key);
  };

  return (
    <div
      style={{ borderRight: '1px solid #e7e7e7' }}
      className="h-screen overflow-y-auto overflow-x-hidden"
    >
      <div className="w-full relative p-5">
        <h1 className="p-5 border border-gray-400 border-solid rounded-lg text-center">
          JRG LOGO
        </h1>
      </div>
      <Menu
        onClick={onClick}
        defaultSelectedKeys={[Routes.analytic]}
        defaultOpenKeys={['sub1']}
        mode="inline"
        items={items}
        selectedKeys={[route.pathname]}
      />
    </div>
  );
};

export default Sidebar;
