/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    BASE_URL_DEV: 'BASE_URL_DEV',
    BASE_URL_PROD: 'BASE_URL_PROD',
  },
  images: {
    domains: [],
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/dashboard/analytic',
        permanent: true,
      },
    ];
  },
};

module.exports = nextConfig;
