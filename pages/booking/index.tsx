import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';
import Head from 'next/head';
import { useRecoilValue } from 'recoil';

import { indexSelector } from 'states';

// modules
import FormDataLayout from 'modules/booking/layout/form-data-layout';
import SelectVehicleRouteLayout from 'modules/booking/layout/select-vehicle-route-layout';

const Booking: NextPageWithLayout = () => {
  const indexState = useRecoilValue(indexSelector);

  console.log(indexState.stepBooking);

  return (
    <>
      <Head>
        <title>Booking</title>
      </Head>
      <div className="w-full h-screen bg-gray-100">
        {indexState.stepBooking === 0 && <SelectVehicleRouteLayout />}
        {indexState.stepBooking === 1 && <FormDataLayout />}
      </div>
    </>
  );
};

Booking.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default Booking;
