import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';
import Head from 'next/head';

const Analytic: NextPageWithLayout = () => {
  return (
    <>
      <Head>
        <title>Analytic</title>
      </Head>
      <div className="w-full h-screen bg-gray-200 flex items-center justify-center">
        <h1 className="capitalize text-lg">analytic</h1>
      </div>
    </>
  );
};

Analytic.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default Analytic;
