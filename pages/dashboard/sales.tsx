import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';
import Head from 'next/head';

const Sales: NextPageWithLayout = () => {
  return (
    <>
      <Head>
        <title>Sales</title>
      </Head>
      <div className="w-full h-screen bg-gray-200 flex items-center justify-center">
        <h1 className="capitalize text-lg">Sales</h1>
      </div>
    </>
  );
};

Sales.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default Sales;
