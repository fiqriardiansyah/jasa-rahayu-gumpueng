import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';

const Index: NextPageWithLayout = () => {
  return (
    <>
      <h1 className="text-red-300">project template</h1>
    </>
  );
};

Index.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default Index;
