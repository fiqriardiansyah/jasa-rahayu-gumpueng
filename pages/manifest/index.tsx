import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';
import Head from 'next/head';
import { useRecoilValue } from 'recoil';

import { indexSelector } from 'states';

import DetailManifest from 'modules/manifest/layout/detail-manifest';
import Manifests from 'modules/manifest/layout/manifests';

const Manifest: NextPageWithLayout = () => {
  const indexState = useRecoilValue(indexSelector);

  return (
    <>
      <Head>
        <title>Manifest</title>
      </Head>
      <div className="w-full h-screen bg-gray-100 p-5">
        {indexState.stepManifest === 0 && <Manifests />}
        {indexState.stepManifest === 1 && <DetailManifest />}
      </div>
    </>
  );
};

Manifest.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default Manifest;
