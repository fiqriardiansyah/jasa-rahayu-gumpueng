import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Modal, notification } from 'antd';
import type { NotificationPlacement } from 'antd/es/notification';
import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';
import Head from 'next/head';
import { useRef, useState } from 'react';

// modules
import ModalAddBusService from 'modules/masterdata/bus-service/modal-add-bus';
import ModalDetailBus from 'modules/masterdata/bus-service/modal-detail-bus';
import ModalEditBusService from 'modules/masterdata/bus-service/modal-edit-bus';
import {
  DataBusService,
  TDataBusService,
} from 'modules/masterdata/bus-service/models';
import BusServiceTable from 'modules/masterdata/bus-service/table';
import Header from 'modules/masterdata/components/header';

const BusService: NextPageWithLayout = () => {
  const [notif, notifContextHolder] = notification.useNotification();

  const [loading, setLoading] = useState({
    add: false,
    delete: false,
    edit: false,
  });
  const editTriggerRef = useRef<HTMLButtonElement | null>(null);
  const detailTriggerRef = useRef<HTMLButtonElement | null>(null);

  const addHandler = (data: DataBusService, callback: () => void) => {
    console.log('addHandler', data);
    setLoading((prev) => ({ ...prev, add: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil disimpan!'
      );
      setLoading((prev) => ({ ...prev, add: false }));
      callback();
    }, 2000);
  };

  const onClickDelete = (data: TDataBusService) => {
    Modal.confirm({
      title: 'Delete',
      icon: <ExclamationCircleOutlined />,
      content: 'Hapus data dengan id ' + data.id + ' ?',
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve('kwk');
            openNotificationSuccess(
              'bottomRight',
              'Berhasil',
              'Data berhasil dihapus'
            );
          }, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
      okButtonProps: {
        danger: true,
      },
    });
  };

  const editHandler = (data: DataBusService, callback: () => void) => {
    console.log('edithandler', data);
    setLoading((prev) => ({ ...prev, edit: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil diubah!'
      );
      setLoading((prev) => ({ ...prev, edit: false }));
      callback();
    }, 2000);
  };

  const onClickEdit = (data: TDataBusService) => {
    if (editTriggerRef.current) {
      editTriggerRef.current.dataset.busservice = JSON.stringify(data);
      editTriggerRef.current.click();
    }
  };

  const onClickDetail = (data: TDataBusService) => {
    if (detailTriggerRef.current) {
      detailTriggerRef.current.dataset.busservice = JSON.stringify(data);
      detailTriggerRef.current.click();
    }
  };

  const openNotificationSuccess = (
    placement: NotificationPlacement,
    title: string,
    description: string
  ) => {
    notif.success({
      message: title,
      description: description,
      placement,
    });
  };

  return (
    <>
      {notifContextHolder}
      <Head>
        <title>Jenis Layanan Bus</title>
      </Head>
      <ModalEditBusService loading={loading.edit} onSubmit={editHandler}>
        {(data) => (
          <button
            ref={editTriggerRef}
            onClick={() =>
              data.openModalWithData(editTriggerRef.current?.dataset.busservice)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalEditBusService>
      <ModalDetailBus>
        {(data) => (
          <button
            ref={detailTriggerRef}
            onClick={() =>
              data.openModalWithData(
                detailTriggerRef.current?.dataset.busservice
              )
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalDetailBus>
      <div className="w-full h-screen flex flex-col">
        <Header
          title="Layanan bus"
          action={
            <ModalAddBusService loading={loading.add} onSubmit={addHandler}>
              {(data) => (
                <Button
                  onClick={data.openModal}
                  type="default"
                  icon={<PlusOutlined />}
                >
                  Tambah Layanan
                </Button>
              )}
            </ModalAddBusService>
          }
        />
        <div className="w-full px-10">
          <BusServiceTable
            onClickDelete={onClickDelete}
            onClickEdit={onClickEdit}
            onClickDetail={onClickDetail}
          />
        </div>
      </div>
    </>
  );
};

BusService.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default BusService;
