import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Modal, notification } from 'antd';
import type { NotificationPlacement } from 'antd/es/notification';
import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';
import Head from 'next/head';
import { useRef, useState } from 'react';

// modules
import Header from 'modules/masterdata/components/header';
import ModalAddCrew from 'modules/masterdata/crew/modal-add-crew';
import ModalDetailCrew from 'modules/masterdata/crew/modal-detail-crew';
import ModalEditCrew from 'modules/masterdata/crew/modal-edit-crew';
import { DataCrew, TDataCrew } from 'modules/masterdata/crew/models';
import CrewTable from 'modules/masterdata/crew/table';

const Crew: NextPageWithLayout = () => {
  const [notif, notifContextHolder] = notification.useNotification();

  const [loading, setLoading] = useState({
    add: false,
    delete: false,
    edit: false,
  });
  const editTriggerRef = useRef<HTMLButtonElement | null>(null);
  const detailTriggerRef = useRef<HTMLButtonElement | null>(null);

  const addHandler = (data: DataCrew, callback: () => void) => {
    console.log('addHandler', data);
    setLoading((prev) => ({ ...prev, add: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil disimpan!'
      );
      setLoading((prev) => ({ ...prev, add: false }));
      callback();
    }, 2000);
  };

  const onClickDelete = (data: TDataCrew) => {
    Modal.confirm({
      title: 'Delete',
      icon: <ExclamationCircleOutlined />,
      content: 'Hapus data dengan id ' + data.id + ' ?',
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve('kwk');
            openNotificationSuccess(
              'bottomRight',
              'Berhasil',
              'Data berhasil dihapus'
            );
          }, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
      okButtonProps: {
        danger: true,
      },
    });
  };

  const editHandler = (data: DataCrew, callback: () => void) => {
    console.log('edithandler', data);
    setLoading((prev) => ({ ...prev, edit: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil diubah!'
      );
      setLoading((prev) => ({ ...prev, edit: false }));
      callback();
    }, 2000);
  };

  const onClickEdit = (data: TDataCrew) => {
    if (editTriggerRef.current) {
      editTriggerRef.current.dataset.crew = JSON.stringify(data);
      editTriggerRef.current.click();
    }
  };

  const onClickDetail = (data: TDataCrew) => {
    if (detailTriggerRef.current) {
      detailTriggerRef.current.dataset.crew = JSON.stringify(data);
      detailTriggerRef.current.click();
    }
  };

  const openNotificationSuccess = (
    placement: NotificationPlacement,
    title: string,
    description: string
  ) => {
    notif.success({
      message: title,
      description: description,
      placement,
    });
  };

  return (
    <>
      {notifContextHolder}
      <Head>
        <title>Data Kru</title>
      </Head>
      <ModalEditCrew loading={loading.edit} onSubmit={editHandler}>
        {(data) => (
          <button
            ref={editTriggerRef}
            onClick={() =>
              data.openModalWithData(editTriggerRef.current?.dataset.crew)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalEditCrew>
      <ModalDetailCrew>
        {(data) => (
          <button
            ref={detailTriggerRef}
            onClick={() =>
              data.openModalWithData(detailTriggerRef.current?.dataset.crew)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalDetailCrew>
      <div className="w-full h-screen flex flex-col">
        <Header
          title="Kru"
          action={
            <ModalAddCrew loading={loading.add} onSubmit={addHandler}>
              {(data) => (
                <Button
                  onClick={data.openModal}
                  type="default"
                  icon={<PlusOutlined />}
                >
                  Tambah Kru
                </Button>
              )}
            </ModalAddCrew>
          }
        />
        <div className="w-full px-10">
          <CrewTable
            onClickDetail={onClickDetail}
            onClickDelete={onClickDelete}
            onClickEdit={onClickEdit}
          />
        </div>
      </div>
    </>
  );
};

Crew.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default Crew;
