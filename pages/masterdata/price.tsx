import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Modal, notification } from 'antd';
import type { NotificationPlacement } from 'antd/es/notification';
import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';
import Head from 'next/head';
import { useRef, useState } from 'react';

// modules
import Header from 'modules/masterdata/components/header';
import ModalAddPrice from 'modules/masterdata/price/modal-add-price';
import ModalDetailPrice from 'modules/masterdata/price/modal-detail-price';
import ModalEditPrice from 'modules/masterdata/price/modal-edit-price';
import { DataPrice, TDataPrice } from 'modules/masterdata/price/models';
import PriceTable from 'modules/masterdata/price/table';

const Price: NextPageWithLayout = () => {
  const [notif, notifContextHolder] = notification.useNotification();

  const [loading, setLoading] = useState({
    add: false,
    delete: false,
    edit: false,
  });
  const editTriggerRef = useRef<HTMLButtonElement | null>(null);
  const detailTriggerRef = useRef<HTMLButtonElement | null>(null);

  const addHandler = (data: DataPrice, callback: () => void) => {
    console.log('addHandler', data);
    setLoading((prev) => ({ ...prev, add: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil disimpan!'
      );
      setLoading((prev) => ({ ...prev, add: false }));
      callback();
    }, 2000);
  };

  const onClickDelete = (data: TDataPrice) => {
    Modal.confirm({
      title: 'Delete',
      icon: <ExclamationCircleOutlined />,
      content: 'Hapus data dengan id ' + data.id + ' ?',
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve('kwk');
            openNotificationSuccess(
              'bottomRight',
              'Berhasil',
              'Data berhasil dihapus'
            );
          }, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
      okButtonProps: {
        danger: true,
      },
    });
  };

  const editHandler = (data: DataPrice, callback: () => void) => {
    console.log('edithandler', data);
    setLoading((prev) => ({ ...prev, edit: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil diubah!'
      );
      setLoading((prev) => ({ ...prev, edit: false }));
      callback();
    }, 2000);
  };

  const onClickEdit = (data: TDataPrice) => {
    if (editTriggerRef.current) {
      editTriggerRef.current.dataset.price = JSON.stringify(data);
      editTriggerRef.current.click();
    }
  };

  const onClickDetail = (data: TDataPrice) => {
    if (detailTriggerRef.current) {
      detailTriggerRef.current.dataset.price = JSON.stringify(data);
      detailTriggerRef.current.click();
    }
  };

  const openNotificationSuccess = (
    placement: NotificationPlacement,
    title: string,
    description: string
  ) => {
    notif.success({
      message: title,
      description: description,
      placement,
    });
  };

  return (
    <>
      {notifContextHolder}
      <Head>
        <title>Data Harga</title>
      </Head>
      <ModalEditPrice loading={loading.edit} onSubmit={editHandler}>
        {(data) => (
          <button
            ref={editTriggerRef}
            onClick={() =>
              data.openModalWithData(editTriggerRef.current?.dataset.price)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalEditPrice>
      <ModalDetailPrice>
        {(data) => (
          <button
            ref={detailTriggerRef}
            onClick={() =>
              data.openModalWithData(detailTriggerRef.current?.dataset.price)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalDetailPrice>
      <div className="w-full h-screen flex flex-col">
        <Header
          title="Harga"
          action={
            <ModalAddPrice loading={loading.add} onSubmit={addHandler}>
              {(data) => (
                <Button
                  onClick={data.openModal}
                  type="default"
                  icon={<PlusOutlined />}
                >
                  Tambah Harga
                </Button>
              )}
            </ModalAddPrice>
          }
        />
        <div className="w-full px-10">
          <PriceTable
            onClickDetail={onClickDetail}
            onClickDelete={onClickDelete}
            onClickEdit={onClickEdit}
          />
        </div>
      </div>
    </>
  );
};

Price.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default Price;
