import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Modal, notification } from 'antd';
import type { NotificationPlacement } from 'antd/es/notification';
import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';
import Head from 'next/head';
import { useRef, useState } from 'react';

// modules
import Header from 'modules/masterdata/components/header';
import ModalAddRoute from 'modules/masterdata/route/modal-add-route';
import ModalDetailRoute from 'modules/masterdata/route/modal-detail-route';
import ModalEditRoute from 'modules/masterdata/route/modal-edit-route';
import { DataRoute, TDataRoute } from 'modules/masterdata/route/models';
import RouteTable from 'modules/masterdata/route/table';

const Route: NextPageWithLayout = () => {
  const [notif, notifContextHolder] = notification.useNotification();

  const [loading, setLoading] = useState({
    add: false,
    delete: false,
    edit: false,
  });
  const editTriggerRef = useRef<HTMLButtonElement | null>(null);
  const detailTriggerRef = useRef<HTMLButtonElement | null>(null);

  const addHandler = (data: DataRoute, callback: () => void) => {
    console.log('addHandler', data);
    setLoading((prev) => ({ ...prev, add: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil disimpan!'
      );
      setLoading((prev) => ({ ...prev, add: false }));
      callback();
    }, 2000);
  };

  const onClickDelete = (data: TDataRoute) => {
    Modal.confirm({
      title: 'Delete',
      icon: <ExclamationCircleOutlined />,
      content: 'Hapus data dengan id ' + data.id + ' ?',
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve('kwk');
            openNotificationSuccess(
              'bottomRight',
              'Berhasil',
              'Data berhasil dihapus'
            );
          }, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
      okButtonProps: {
        danger: true,
      },
    });
  };

  const editHandler = (data: DataRoute, callback: () => void) => {
    console.log('edithandler', data);
    setLoading((prev) => ({ ...prev, edit: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil diubah!'
      );
      setLoading((prev) => ({ ...prev, edit: false }));
      callback();
    }, 2000);
  };

  const onClickEdit = (data: TDataRoute) => {
    if (editTriggerRef.current) {
      editTriggerRef.current.dataset.route = JSON.stringify(data);
      editTriggerRef.current.click();
    }
  };

  const onClickDetail = (data: TDataRoute) => {
    if (detailTriggerRef.current) {
      detailTriggerRef.current.dataset.route = JSON.stringify(data);
      detailTriggerRef.current.click();
    }
  };

  const openNotificationSuccess = (
    placement: NotificationPlacement,
    title: string,
    description: string
  ) => {
    notif.success({
      message: title,
      description: description,
      placement,
    });
  };

  return (
    <>
      {notifContextHolder}
      <Head>
        <title>Trayek</title>
      </Head>
      <ModalEditRoute loading={loading.edit} onSubmit={editHandler}>
        {(data) => (
          <button
            ref={editTriggerRef}
            onClick={() =>
              data.openModalWithData(editTriggerRef.current?.dataset.route)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalEditRoute>
      <ModalDetailRoute>
        {(data) => (
          <button
            ref={detailTriggerRef}
            onClick={() =>
              data.openModalWithData(detailTriggerRef.current?.dataset.route)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalDetailRoute>
      <div className="w-full h-screen flex flex-col">
        <Header
          title="Trayek"
          action={
            <ModalAddRoute loading={loading.add} onSubmit={addHandler}>
              {(data) => (
                <Button
                  onClick={data.openModal}
                  type="default"
                  icon={<PlusOutlined />}
                >
                  Tambah Trayek
                </Button>
              )}
            </ModalAddRoute>
          }
        />
        <div className="w-full px-10">
          <RouteTable
            onClickDetail={onClickDetail}
            onClickDelete={onClickDelete}
            onClickEdit={onClickEdit}
          />
        </div>
      </div>
    </>
  );
};

Route.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default Route;
