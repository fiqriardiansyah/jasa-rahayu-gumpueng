import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Modal, notification } from 'antd';
import type { NotificationPlacement } from 'antd/es/notification';
import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';
import Head from 'next/head';
import { useRef, useState } from 'react';

// modules
import Header from 'modules/masterdata/components/header';
import ModalAddStation from 'modules/masterdata/station/modal-add-station';
import ModalDetailStation from 'modules/masterdata/station/modal-detail-station';
import ModalEditStation from 'modules/masterdata/station/modal-edit-station';
import { DataStation, TDataStation } from 'modules/masterdata/station/models';
import StationTable from 'modules/masterdata/station/table';

const Station: NextPageWithLayout = () => {
  const [notif, notifContextHolder] = notification.useNotification();

  const [loading, setLoading] = useState({
    add: false,
    delete: false,
    edit: false,
  });
  const editTriggerRef = useRef<HTMLButtonElement | null>(null);
  const detailTriggerRef = useRef<HTMLButtonElement | null>(null);

  const addHandler = (data: DataStation, callback: () => void) => {
    console.log('addHandler', data);
    setLoading((prev) => ({ ...prev, add: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil disimpan!'
      );
      setLoading((prev) => ({ ...prev, add: false }));
      callback();
    }, 2000);
  };

  const onClickDelete = (data: TDataStation) => {
    Modal.confirm({
      title: 'Delete',
      icon: <ExclamationCircleOutlined />,
      content: 'Hapus data dengan id ' + data.id + ' ?',
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve('kwk');
            openNotificationSuccess(
              'bottomRight',
              'Berhasil',
              'Data berhasil dihapus'
            );
          }, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
      okButtonProps: {
        danger: true,
      },
    });
  };

  const editHandler = (data: DataStation, callback: () => void) => {
    console.log('edithandler', data);
    setLoading((prev) => ({ ...prev, edit: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil diubah!'
      );
      setLoading((prev) => ({ ...prev, edit: false }));
      callback();
    }, 2000);
  };

  const onClickEdit = (data: TDataStation) => {
    if (editTriggerRef.current) {
      editTriggerRef.current.dataset.station = JSON.stringify(data);
      editTriggerRef.current.click();
    }
  };

  const onClickDetail = (data: TDataStation) => {
    if (detailTriggerRef.current) {
      detailTriggerRef.current.dataset.station = JSON.stringify(data);
      detailTriggerRef.current.click();
    }
  };

  const openNotificationSuccess = (
    placement: NotificationPlacement,
    title: string,
    description: string
  ) => {
    notif.success({
      message: title,
      description: description,
      placement,
    });
  };

  return (
    <>
      {notifContextHolder}
      <Head>
        <title>Data loket</title>
      </Head>
      <ModalEditStation loading={loading.edit} onSubmit={editHandler}>
        {(data) => (
          <button
            ref={editTriggerRef}
            onClick={() =>
              data.openModalWithData(editTriggerRef.current?.dataset.station)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalEditStation>
      <ModalDetailStation>
        {(data) => (
          <button
            ref={detailTriggerRef}
            onClick={() =>
              data.openModalWithData(detailTriggerRef.current?.dataset.station)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalDetailStation>
      <div className="w-full h-screen flex flex-col">
        <Header
          title="Loket"
          action={
            <ModalAddStation loading={loading.add} onSubmit={addHandler}>
              {(data) => (
                <Button
                  onClick={data.openModal}
                  type="default"
                  icon={<PlusOutlined />}
                >
                  Tambah Loket
                </Button>
              )}
            </ModalAddStation>
          }
        />
        <div className="w-full px-10">
          <StationTable
            onClickDetail={onClickDetail}
            onClickDelete={onClickDelete}
            onClickEdit={onClickEdit}
          />
        </div>
      </div>
    </>
  );
};

Station.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default Station;
