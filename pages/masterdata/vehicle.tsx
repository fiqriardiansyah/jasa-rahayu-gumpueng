import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Modal, notification } from 'antd';
import type { NotificationPlacement } from 'antd/es/notification';
import LayoutWithSidenav from 'layout/layout-with-sidenav';
import { NextPageWithLayout } from 'models/props';
import Head from 'next/head';
import { useRef, useState } from 'react';

// modules
import Header from 'modules/masterdata/components/header';
import ModalAddVehicle from 'modules/masterdata/vehicle/modal-add-vehicle';
import ModalDetailVehicle from 'modules/masterdata/vehicle/modal-detail-vehicle';
import ModalEditVehicle from 'modules/masterdata/vehicle/modal-edit-vehicle';
import { DataVehicle, TDataVehicle } from 'modules/masterdata/vehicle/models';
import VehicleTable from 'modules/masterdata/vehicle/table';

const Vehicle: NextPageWithLayout = () => {
  const [notif, notifContextHolder] = notification.useNotification();

  const [loading, setLoading] = useState({
    add: false,
    delete: false,
    edit: false,
  });
  const editTriggerRef = useRef<HTMLButtonElement | null>(null);
  const detailTriggerRef = useRef<HTMLButtonElement | null>(null);

  const addHandler = (data: DataVehicle, callback: () => void) => {
    console.log('addHandler', data);
    setLoading((prev) => ({ ...prev, add: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil disimpan!'
      );
      setLoading((prev) => ({ ...prev, add: false }));
      callback();
    }, 2000);
  };

  const onClickDelete = (data: TDataVehicle) => {
    Modal.confirm({
      title: 'Delete',
      icon: <ExclamationCircleOutlined />,
      content: 'Hapus data dengan id ' + data.id + ' ?',
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve('kwk');
            openNotificationSuccess(
              'bottomRight',
              'Berhasil',
              'Data berhasil dihapus'
            );
          }, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
      okButtonProps: {
        danger: true,
      },
    });
  };

  const editHandler = (data: DataVehicle, callback: () => void) => {
    console.log('edithandler', data);
    setLoading((prev) => ({ ...prev, edit: true }));
    setTimeout(() => {
      openNotificationSuccess(
        'bottomRight',
        'Berhasil',
        'Data berhasil diubah!'
      );
      setLoading((prev) => ({ ...prev, edit: false }));
      callback();
    }, 2000);
  };

  const onClickEdit = (data: TDataVehicle) => {
    if (editTriggerRef.current) {
      editTriggerRef.current.dataset.vehicle = JSON.stringify(data);
      editTriggerRef.current.click();
    }
  };

  const onClickDetail = (data: TDataVehicle) => {
    if (detailTriggerRef.current) {
      detailTriggerRef.current.dataset.vehicle = JSON.stringify(data);
      detailTriggerRef.current.click();
    }
  };

  const openNotificationSuccess = (
    placement: NotificationPlacement,
    title: string,
    description: string
  ) => {
    notif.success({
      message: title,
      description: description,
      placement,
    });
  };

  return (
    <>
      {notifContextHolder}
      <Head>
        <title>Data kendaraan</title>
      </Head>
      <ModalEditVehicle loading={loading.edit} onSubmit={editHandler}>
        {(data) => (
          <button
            ref={editTriggerRef}
            onClick={() =>
              data.openModalWithData(editTriggerRef.current?.dataset.vehicle)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalEditVehicle>
      <ModalDetailVehicle>
        {(data) => (
          <button
            ref={detailTriggerRef}
            onClick={() =>
              data.openModalWithData(detailTriggerRef.current?.dataset.vehicle)
            }
            className="w-0 h-0 hidden"
          />
        )}
      </ModalDetailVehicle>
      <div className="w-full h-screen flex flex-col">
        <Header
          title="Kendaraan"
          action={
            <ModalAddVehicle loading={loading.add} onSubmit={addHandler}>
              {(data) => (
                <Button
                  onClick={data.openModal}
                  type="default"
                  icon={<PlusOutlined />}
                >
                  Tambah Kendaraan
                </Button>
              )}
            </ModalAddVehicle>
          }
        />
        <div className="w-full px-10">
          <VehicleTable
            onClickDetail={onClickDetail}
            onClickDelete={onClickDelete}
            onClickEdit={onClickEdit}
          />
        </div>
      </div>
    </>
  );
};

Vehicle.getLayout = (page) => {
  return <LayoutWithSidenav>{page}</LayoutWithSidenav>;
};

export default Vehicle;
