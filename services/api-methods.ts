import { AxiosResponse } from 'axios';
import client from 'configurations/axios';
import { BaseResponse, GetMethodParams } from 'models';

export default class ApiMethod {
    static async get<T>(data: GetMethodParams): Promise<AxiosResponse<BaseResponse<T>, any>> {
        return await client.get(data.url, data?.config);
    }
}
