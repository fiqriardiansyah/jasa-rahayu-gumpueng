import { atom, selector } from 'recoil';

import { BookingData } from 'models';
import { SeatLayout } from 'modules/booking/models';

export type IndexAtomType = {
    bookingData: BookingData | null,
    seat: SeatLayout | null,
    stepBooking: number,
    stepManifest: number,
}

export const index = atom({
    key: 'index',
    default: {
        bookingData: null,
        seat: null,
        stepBooking: 0,
        stepManifest: 0,
    } as IndexAtomType,
});

export const indexSelector = selector<IndexAtomType>({
    key: 'indexSelector',
    get: ({ get }): IndexAtomType => {
        const indexAtom = get(index);
        return {
            ...indexAtom,
        };
    },
    set: ({ set, get }, value) => {
        const data: IndexAtomType = value as IndexAtomType;
        set(index, data);
    },
});
