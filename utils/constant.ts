export const TOKEN_USER = 'token';
export const locale = "en-us";

export const currency = [
    {
        CtryNm: 'INDONESIA',
        CcyNm: 'Rupiah',
        Ccy: 'IDR',
        CcyNbr: '971',
        CcyMnrUnts: '2',
    }
]