import { SeatLayout } from "modules/booking/models";
import { locale } from "./constant";

export default class Utils {
    static charCodeA = 65;

    static convertToStringFormat(num?: number | null): string {
        if (num === null || num === undefined) return '-';
        const mergeNum = num?.toString().split('.').join('');
        return mergeNum.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    }

    static convertToIntFormat(str?: string): number {
        if (str === null || str === undefined) return 0;
        return parseInt(str.toString().split('.').join(''), 10);
    }

    static cutText(length: number, string?: string): string {
        if (!string) return '-';
        return string?.length > length ? `${string.slice(0, length)}...` : string;
    }

    static imageSafety(image: string | undefined | null): string {
        return image ?? '/images/placeholder.png';
    }

    static currencyFormatter = (selectedCurrOpt: any) => (value: any) => {
        return new Intl.NumberFormat(locale, {
            style: "currency",
            currency: selectedCurrOpt.split("::")[1]
        }).format(value);
    };

    static currencyParser = (val: any) => {
        try {
            // for when the input gets clears
            if (typeof val === "string" && !val.length) {
                val = "0.0";
            }

            // detecting and parsing between comma and dot
            var group = new Intl.NumberFormat(locale).format(1111).replace(/1/g, "");
            var decimal = new Intl.NumberFormat(locale).format(1.1).replace(/1/g, "");
            var reversedVal = val.replace(new RegExp("\\" + group, "g"), "");
            reversedVal = reversedVal.replace(new RegExp("\\" + decimal, "g"), ".");
            //  => 1232.21 €

            // removing everything except the digits and dot
            reversedVal = reversedVal.replace(/[^0-9.]/g, "");
            //  => 1232.21

            // appending digits properly
            const digitsAfterDecimalCount = (reversedVal.split(".")[1] || []).length;
            const needsDigitsAppended = digitsAfterDecimalCount > 2;

            if (needsDigitsAppended) {
                reversedVal = reversedVal * Math.pow(10, digitsAfterDecimalCount - 2);
            }

            return Number.isNaN(reversedVal) ? 0 : reversedVal;
        } catch (error) {
            console.error(error);
        }
    };

    static generateGridArea = (layout: SeatLayout) => {
        let gridArea: string[] = [];
        layout.layout.forEach((row, indexRow) => {
            const nameArea: string[] = [];
            row.forEach((seats, indexSeats) => {
                seats.forEach(() => {
                    nameArea.push(
                        `${String.fromCharCode(this.charCodeA + indexRow)}${indexRow}${indexSeats}`
                    );
                });
            });
            gridArea.push(`"${nameArea.join(' ')}"`);
        });

        return gridArea.join(' ');
    };

}
