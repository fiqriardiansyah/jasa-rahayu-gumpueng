class Routes {
    // dashboard
    static analytic = '/dashboard/analytic';
    static sales = '/dashboard/sales';

    // master data
    static route = '/masterdata/route';
    static busService = '/masterdata/bus-service';
    static price = '/masterdata/price';
    static vehicle = '/masterdata/vehicle';
    static station = '/masterdata/station';
    static crew = '/masterdata/crew';

    // booking
    static booking = '/booking';

    // manifest
    static manifest = '/manifest';
}

export default Routes;